# Description: This script demonstrates the processing of a single hole GPR data set.
# The data is loaded, the mean is removed, the clipping is interpolated, a bandpass filter is applied,
# The direct wave is removed using SVD decomposition and horizontal filtering
# Finally, a linear time gain is applied
# The data is plotted at each step.

# %% Load the needed modules
from gdp import DATA_DIR # our data directory
from pathlib import Path # for handling file paths
from gdp.import_export.import_gpr_data import load_mala
from gdp.processing import filtering
from gdp.processing import general
from gdp.plotting.plot import (plot_frequency_information,
                               plot_image_comparison,
                               plot_trace_comparison)

# %% Load the data and print the header
data_path = Path(DATA_DIR + 'test_data/mala_gpr_singlehole_data.rd3')
raw_data, header = load_mala(str(data_path))
print(header)
travel_time = header['travel time (ns)']
dt = header['time step (ns)']
test_trace = 100

plot_frequency_information(raw_data[:, test_trace], dt)

# %% Remove the mean of each trace using a time window (start to end)

no_mean_data, removed_part = filtering.remove_mean(raw_data,
                                                   start_index=1,
                                                   end_index=30)

plot_frequency_information(no_mean_data[:, test_trace], dt)

plot_image_comparison(im_before=raw_data,
                      im_after=no_mean_data,
                      removed=removed_part)

plot_trace_comparison(im_before=raw_data,
                      im_after=no_mean_data,
                      trace=test_trace)

# %% Interpolate the clipping seen on the data to facilitate further processing


no_clip_data, _ = general.interpolate_clipping(no_mean_data, downscale_factor=0.99)

plot_trace_comparison(im_before=no_mean_data,
                      im_after=no_clip_data,
                      trace=test_trace)

# %% Apply a bandpass filter to the data
bandpass_data = filtering.filter_data(no_clip_data,
                                      fq=[1, 150],
                                      sfreq=header['frequency (MHz)'],
                                      btype='bandpass')

plot_trace_comparison(im_before=no_clip_data,
                      im_after=bandpass_data,
                      trace=test_trace)

# %% Compare to simply a lowpass filter
lowpass_data = filtering.filter_data(no_clip_data,
                                      fq=300,
                                      sfreq=header['frequency (MHz)'],
                                      btype='lowpass')

plot_trace_comparison(im_before=no_clip_data,
                      im_after=lowpass_data,
                      trace=test_trace)

plot_frequency_information(no_clip_data[:, test_trace], dt)
plot_frequency_information(lowpass_data[:, test_trace], dt)

# %% Remove the direct wave with SVD decomposition
from gdp.processing.image_processing import remove_svd

svd_data, removed_part_svd = remove_svd(lowpass_data,
                                        low_s=0,
                                        high_s=1)

plot_image_comparison(im_before=lowpass_data,
                      im_after=svd_data,
                      removed=removed_part_svd)
# %% Remove the direct wave with horizontal filtering
from gdp.processing.filtering import horizontal_filter

horizontal_filter_data, removed_part_hf = horizontal_filter(lowpass_data,
                                           half_window=500)

plot_image_comparison(im_before=lowpass_data,
                      im_after=horizontal_filter_data,
                      removed=removed_part_hf)

# %% Apply a linear time gain to the data
from gdp.processing import gain
exponent = 2.5
gain_data, gain_matrix = gain.apply_gain(svd_data,
                                         sfreq=header['frequency (GHz)'],
                                         gain_type='linear',
                                         exponent=exponent)
plot_image_comparison(im_before=svd_data,
                      im_after=gain_data,
                      removed=gain_matrix)
