# This script downloads GPR data recorded from a MALA system from the ETH polybox.
# It saves them in the test_data folder in the data directory.

# %% import the needed functions
from gdp.data_download import download_data_from_url
from gdp.helpers.common_functions import create_directory
from gdp import DATA_DIR

# Create the directory if it does not exist
create_directory(DATA_DIR + 'test_data')

# %% Download cross-hole data from MB8-MB5 boreholes (Bedretto Lab).
# Source is fixed and receiver is moved along the borehole.

#%% Define the url's to download the data
data_rd3_file = 'https://polybox.ethz.ch/index.php/s/Hw8gs4onc0XRDSS/download'
data_rad_file = 'https://polybox.ethz.ch/index.php/s/5AnHHVbALmJdEu1/download'

download_data_from_url(url=data_rd3_file,
                       filename='mala_gpr_crosshole_data.rd3',
                       target_folder=DATA_DIR + 'test_data')
download_data_from_url(url=data_rad_file,
                       filename='mala_gpr_crosshole_data.rad',
                       target_folder=DATA_DIR + 'test_data')

# %% Download single-hole data from CB1 borehole, 300 m long (Bedretto Lab).
# See Shakas et al. (2020). Permeability enhancement from a hydraulic stimulation imaged with Ground Penetrating Radar.
# Geophysical Research Letters, 47(17), e2020GL088783.

#%% Define the url's to download the data
data_rd3_file = 'https://polybox.ethz.ch/index.php/s/lhpvqFhcK5tXOrt/download'
data_rad_file = 'https://polybox.ethz.ch/index.php/s/WWy5HaHuzr9CW5w/download'
#%%
download_data_from_url(url=data_rd3_file,
                       filename='mala_gpr_singlehole_data.rd3',
                       target_folder=DATA_DIR + 'test_data')
download_data_from_url(url=data_rad_file,
                       filename='mala_gpr_singlehole_data.rad',
                       target_folder=DATA_DIR + 'test_data')
