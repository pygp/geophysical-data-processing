# This script downloads active seismic data recorded with a sparker and hydrophones from GeoTomographie.
# The data is stored as a SEG-2 file
# It saves them in the test_data folder in the data directory.

# %% import the needed functions
from gdp.data_download import download_data_from_url
from gdp.helpers.common_functions import create_directory
from gdp import DATA_DIR

#%% Define the url's to download the data
data_file = 'https://polybox.ethz.ch/index.php/s/3PG7TFXpJjflhhv/download'

#%%
create_directory(DATA_DIR + 'test_data')

download_data_from_url(url=data_file,
                       filename='active_seismic_data.dat',
                       target_folder=DATA_DIR + 'test_data')


