# This script uses the pick_roi function to pick regions of interest from a GPR image.
# The roi's are then plotted on top of the image, and also as individual subplots.
# The tool works by using the mouse buttons (see header of plot window for instructions).

# %% Importing modules
import matplotlib.pyplot as plt
from gdp.analysis.picking import pick_roi
from gdp import DATA_DIR
from gdp.import_export.import_gpr_data import load_mala
from gdp.processing.filtering import filter_data
from gdp.plotting.plot import plot_rectangle

# %% Load the data and apply a highpass filter to remove DC shift
data, info = load_mala(DATA_DIR + 'test_data/mala_gpr_crosshole_data.rd3')
data = filter_data(data, 10, info['frequency (MHz)'], btype='highpass')

# %% Plot the image using the picker and pick
extent = (0, 1, 0, 1)
output = pick_roi(data,
                   extent=extent,
                   colormap='seismic',
                   automatic_naming=True,
                   save_image_crop=True,
                           )

# %% Plot the image and the picked roi's
num_roi = len(output)
fig, axs = plt.subplots(1, num_roi + 1)
axs = axs.ravel()
axs[0].set_title('Image with picked ROIs')
im = axs[0].imshow(data,
                   extent=extent,
                   cmap='seismic')
counter = 1
for roi_name, roi in output.items():
    rectangle = roi['true coordinates']
    kwargs = {'color': 'red', 'linewidth': 2, 'label': roi_name}
    axs[0] = plot_rectangle(rectangle, axs[0], kwargs)
    axs[counter].set_title(roi_name)
    axs[counter].imshow(roi['data'],
                        cmap='seismic')
    counter += 1


