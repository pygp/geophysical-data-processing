# This script uses the pick_line function to pick line(s) from a GPR image.
# The line(s) are then plotted on top of the image, and also the values along the line(s).
# The picker works by using the mouse buttons (see header of plot window for instructions).

# %% Importing modules
import matplotlib.pyplot as plt
from gdp.analysis.picking import pick_line
from gdp import DATA_DIR
from gdp.import_export.import_gpr_data import load_mala
from gdp.processing.filtering import filter_data

# %% Load the data and apply a highpass filter to remove DC shift
data, info = load_mala(DATA_DIR + 'test_data/mala_gpr_crosshole_data.rd3')
data = filter_data(data, 10, info['frequency (MHz)'], btype='highpass')

# %% Plot the image using the picker and pick
extent = (0, 1, 0, 1)
output = pick_line(data,
                   extent=extent,
                   colormap='seismic',
                   interpolate_path=True,
                   automatic_naming=True,
                   compute_lengths=True,
                   compute_zero_crossing=0.2,
                   values_along_path=True)

# %% Plot the image and the picked lines
fig, axs = plt.subplots(2, 1)
im = axs[0].imshow(data,
                   extent=extent,
                   cmap='seismic')
for pick_name, item in output.items():
    transform = item['true coordinates']
    if transform.any():
        axs[0].plot(transform[:, 0],
                    transform[:, 1],
                    label=pick_name)
        axs[1].plot(item['path distance'],
                    item['path values'],
                    label=pick_name)
plt.show()