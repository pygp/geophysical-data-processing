import numpy as np


def normalize_data(D_in, typ='max-abs', window=None):
    """
    Function that normalizes a data matrix 'D_in' by a certain criterion given by inputs 'type' and 'window'
    Args:
        D_in: Data in (samples, traces)
        typ: ['tracewise-max-abs',
               'tracewise-rms',
               'tracewise-max-abs-window',
               'tracewise-rms-window',
               'max-abs',
               'rms-sum',
               'max',
               'rms'
               ]
        window: (int, int)

    Returns:
         Normalized data out [samples, traces]
    """
    if D_in.ndim == 1:
        D_in = D_in[np.newaxis].T

    samples, traces = D_in.shape
    if window is None:
        window = (0, samples)
        print('No window defined for normalization. If a window is needed, the complete trace will be used')
    options = ['tracewise-max-abs',
               'tracewise-rms',
               'tracewise-max-abs-window',
               'tracewise-rms-window',
               'max-abs',
               'rms-sum',
               'max',
               'rms'
               ]
    no_nans = ~np.isnan(D_in)
    if typ == 'tracewise-max-abs':
        Norm_D = np.tile(np.max(np.abs(D_in), axis=0), (samples, 1))
        D_out = np.divide(D_in, Norm_D, out=np.zeros_like(D_in), where=Norm_D!=0)

    elif typ == 'tracewise-rms':
        Norm_D = np.tile(np.sqrt(np.mean(D_in**2, axis=0)), (samples, 1))
        D_out = np.divide(D_in, Norm_D, out=np.zeros_like(D_in), where=Norm_D!=0)
    
    elif typ == 'tracewise-max-abs-window':
        Norm_D = np.tile(np.max(np.abs(D_in[window[0]:window[1], :]), axis=0), (samples, 1))
        D_out = np.divide(D_in, Norm_D, out=np.zeros_like(D_in), where=Norm_D!=0)
        
    elif typ == 'tracewise-rms-window':
        Norm_D = np.tile(np.sqrt(np.mean(D_in[window[0]:window[1], :]**2, axis=0)), (samples, 1))
        D_out = np.divide(D_in, Norm_D, out=np.zeros_like(D_in), where=Norm_D!=0)
    
    elif typ=='max-abs':
        D_out = D_in/np.max(np.abs(D_in[no_nans]))

    elif typ=='rms-sum':
        D_out = D_in/np.sum(np.sqrt(np.mean(D_in[no_nans]**2, axis=0)))

    elif typ=='max':
        D_out = D_in/np.max(D_in[no_nans])

    elif typ=='rms':
        D_out = D_in/np.sqrt(np.mean(D_in[no_nans]**2))
    else:
        raise AttributeError('{} as type not recognized. Options are:'.format(typ, options))
    # D_out[np.isnan(D_out)] = 0
    return D_out


