import numpy as np
import scipy.signal as signal

def horizontal_filter(data: np.ndarray,
                      half_window: int = 5,
                      ) -> (np.ndarray, np.ndarray):
    """Apply a horizontal filter on the data through a simple moving average filter along the columns.
    The moving average takes a window of size 2*half_window + 1
    Args:
        data: 2D array to filter
        half_window: Half window size
    Returns:
        Filtered data
        Removed part of the data
    """
    data_copy = data.copy()
    for i in range(data.shape[1]):
        if i < half_window:
            data_copy[:, i] = data[:, i] - np.nanmean(data[:, 0:2 * half_window], axis=1)
        elif i > data.shape[1] - half_window:
            data_copy[:, i] = data[:, i] - np.nanmean(data[:, i - 2 * half_window:], axis=1)
        else:
            data_copy[:, i] = data[:, i] - np.nanmean(data[:, i - half_window:i + half_window], axis=1)
    return data_copy, data_copy - data


def filter_data(data: np.ndarray,
                fq: float or tuple(float, float),
                sfreq: float,
                btype: str,
                N: int = 4,
                filter_type: str = 'acausal',
                interpolate_nans: bool = False) -> np.ndarray:
    """
    Uses a Butterworth digital filter to create the filter coefficients and the Filter
    data along one-dimension.
    Args:
        data: Matrix or Trace to filter
        fq: Corner Frequency. If 'bandpass' then 2 corner frequencies need to be specified
        sfreq: Sampling frequency
        btype: ['lowpass', 'highpass', 'bandpass']
        N: The order of the filter
        filter_type: 'acausal' or 'causal'.
        interpolate_nans: If True, it will interpolate nans in the signal with 1D interpolation
    Returns:
        Filtered data
    See also:
        scipy.signal.butter
        scipy.signal.filtfilt - 'acausal'
        scipy.signal.lfilter - 'causal'
    """
    if btype == 'bandpass':
        assert len(fq) == 2, 'bandpass filter requires a low and high frequency interval'
        fq = np.asarray(fq)
    elif btype == 'lowpass' or btype == 'highpass':
        assert isinstance(fq, float) or isinstance(fq, int), \
            'Frequency must be an scalar value. {} is not valid'.format(fq)
    else:
        raise AttributeError('{} not recognized as a valid btype'.format(btype))
    nyquistfq = sfreq / 2
    num, denom = signal.butter(N, fq / nyquistfq, btype=btype)  # Normalize by nyquist
    if interpolate_nans:
        # Deal with nans in the signal
        data_no_nan = np.copy(data)
        nans = np.isnan(data_no_nan)
        data[nans] = np.interp(np.flatnonzero(nans), np.flatnonzero(~nans), data_no_nan[~nans])

    if filter_type == 'causal':
        return signal.lfilter(num, denom, data, axis=0)
    elif filter_type == 'acausal':
        return signal.filtfilt(num, denom, data, axis=0)
    else:
        raise NotImplementedError('filter_type: {} not recognized'.format(filter_type))

def remove_mean(data: np.ndarray,
                start_index: int,
                end_index: int,
                ) -> np.ndarray:
    """
    Mean removal using a given range to compute the mean. Note that index=0 is
    the first sample
    Args:
        data: Matrix or Trace to remove mean from
        start_index: sample index where to start computing mean
        end_index: sample index where to end computing mean
    Returns:
        data with mean removed, mean matrix
    """
    samples, traces = data.shape
    mean_matrix = np.tile(np.mean(data[start_index:end_index, :], axis=0), [samples, 1])
    return data - mean_matrix, mean_matrix


def detrend(dat, typ='linear'):
    """
    SciPy provides the function detrend to remove a constant or
    linear trend in a data series in order to see effect of higher order.
    Args:
        dat: (samples, traces)
        typ: ['linear', 'constant']
    Returns:
        dat

    """
    return signal.detrend(dat, type=typ, axis=0)


def dewow(data, window_length):
    """
    Applies dewow (baseline correction) to GPR data using a moving average filter.
    
    Parameters:
    - data: 1D or 2D array of GPR traces (one trace per column in 2D)
    - window_length: The length of the moving window (in samples) for calculating the moving average.
    
    Returns:
    - Corrected data after dewow.
    """
    if len(data.shape) == 1:  # If input is a single trace (1D)
        baseline = signal.convolve(data, np.ones(window_length)/window_length, mode='same', method='auto')
        return data - baseline
    else:  # If input is multiple traces (2D)
        baseline = np.apply_along_axis(
            lambda x: signal.convolve(x, np.ones(window_length)/window_length, mode='same', method='auto'), 
            axis=0, 
            arr=data
        )
        return data - baseline





