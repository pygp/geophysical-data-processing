from .normalizing import normalize_data
from .gain import apply_gain, agc_2D, agc_1D
from .filtering import filter_data, detrend, remove_mean, dewow
from .processing import taper, pad, fft_trace, ifft_trace