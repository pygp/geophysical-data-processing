import numpy as np
from scipy import linalg


def crosscorelate_images_vertically(reference: np.array,
                                    image: np.array,
                                    padding: int = 40):
    '''Take in two images and crosscorrelate them vertically to find the maximum overlap.
    Shift the second image (image) to optimally align with the first (reference).
    Args:
        reference: 2D numpy array used as reference
        image: 2D numpy array that will be shifted vertically
        padding: added to the sides of image to do the cross correlation

    Returns:
        shifted image: optimally shifted image to overlap with reference
        shift_vector: vector with all computed cross correlations
        min_shift: shifts applied per column to image
    '''
    image_size = image.shape
    padded_image = np.pad(image, [(padding, padding), (0, 0)])
    shift_vector = np.zeros(padding * 2 + 1)
    x_range = np.linspace(-padding, padding, 2 * padding + 1).astype(int)
    for xind, xshift in enumerate(x_range):
        nopad = padded_image[padding + xshift:image_size[0] + padding + xshift, :]
        shift_vector[xind] = np.nansum(np.abs(nopad.ravel() - reference.ravel()))
    min_shift = x_range[np.argmin(shift_vector)]
    return padded_image[padding + min_shift:image_size[0] + padding + min_shift, :], shift_vector, min_shift




def remove_svd(input_data, low_s=0, high_s=1):
    """
    Remove an svd-decomposed version of the data from the initial data.
    Args:
        input_data: 2D array of the data
        low_s: smaller singular value to keep
        high_s: largest singular value to keep
    Returns:
        svd_filterd: initial data with svd removed
        svd_decomp: decomposed part that is removed

    """
    U, s, Vh = linalg.svd(input_data)
    sigma = np.zeros(input_data.shape)
    for i in range(low_s, high_s):
        sigma[i, i] = s[i]
    svd_decomp = np.dot(U, np.dot(sigma, Vh))
    return input_data - svd_decomp, svd_decomp

def shift_image(image, xy_p=None, xy_t=None, y_pos=0, depth=None, time_vector=None):
    """
    Flatten the image
    Args:
        image: Profile to flatten
        xy_p: Pixel coordinates
        xy_t: True coordinates
        y_pos: Position to shift the image
        depth: Depth vector
        time_vector: Time vector
    Returns:
        flattened, new_depth
    """

    if xy_p is not None:
        max_y = image.shape[0]
        empty = np.zeros(image.shape)
        for xy in xy_p:

            shift = xy[1] - y_pos
            add = False
            if shift < 0:
                # shift = 0
                add = True
            if add:
                y_range = slice(0, max_y)
                new_t = image[:, xy[0]]
                y_new_range = slice(np.abs(shift), len(new_t))
                empty[y_new_range, xy[0]] = image[y_range, xy[0]][:shift]
            else:
                y_range = slice(shift, max_y)
                new_t = image[y_range, xy[0]]
                y_new_range = slice(0, len(new_t))
                empty[y_new_range, xy[0]] = image[y_range, xy[0]]

        flattened = empty[:, xy_p[:, 0].min():xy_p[:, 0].max()]

        new_depth = depth[xy_p[:, 0].min():xy_p[:, 0].max()]
        return flattened, new_depth
    elif xy_t is not None and depth is not None and time_vector is not None:
        extra = np.abs(np.min(time_vector))
        mask_depth = (min(xy_t[:,0]) <= depth) & (depth <= max(xy_t[:, 0]))
       
        tv=time_vector+extra
        empty = np.zeros(image.shape)
        for xy in xy_t:
            shift = xy[1] - y_pos  #+ extra
            add = False
            
            posx = np.argmin(np.abs(depth - xy[0]))
            if shift < 0:
                add = True
                pos = np.argmin(np.abs(tv - np.abs(shift)))
            if add and pos!=0:
                
                
                new_t = image[:, posx]

                new_t = np.concatenate((np.zeros(pos), new_t[:-pos]))

                empty[:, posx] = new_t
            else:
                pos = np.argmin(np.abs(tv - shift))
                y_range = slice(pos, len(tv))
                new_t = image[y_range, posx]
                y_new_range = slice(0, len(new_t))
                empty[y_new_range, posx] = new_t

        new_depth = depth[mask_depth]
        
        flattened = empty[:, mask_depth]
        return flattened, new_depth
    else:
        raise AttributeError('Either xy_p or xy_t, depth and time_vector must be provided')