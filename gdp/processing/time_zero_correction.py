import numpy as np
import matplotlib.pyplot as plt
from nptyping import NDArray, Shape, Float
from typing import List

def time_zero_correction(distances: list,
                         data_matrix: list,
                         threshold: float = 0.15,
                         plot: bool = False) -> int:
    """
    Input the distances and the filenames and this will return the time_zero trace to remove
    Args:
        distances: list of distances used for the correction
        data_matrix: list of 2-D arrays corresponding to the data file
        threshold: The value to look for the crossing
        plot: whether to plot analysis or not
    Returns:
        index sample to remove

    """
    all_files = [(distances[i], data_matrix[i]) for i in range(len(distances))]
    from gdp.processing.image_processing import pick_first_arrival
    points = []
    traces = []
    samples = []
    for distance, dat in all_files:
        # arrival = pick_first_arrival(dat, threshold=threshold)
        # median is more robust than mean in this case
        # sample = np.round(np.median(arrival[:, 1])).astype(int)
        # TODO: I find this approach better than above because sometimes the close distances have early arriving energy
        sample = find_sample_above_threshold(trace=np.median(dat, axis=1), threshold=threshold)
        points.append((distance, sample))
        samples.append(sample)
        traces.append(np.median(dat, axis=1))
        if show_plots:
            vmax = dat.max()
            fig, ax = plt.subplots(figsize=(10, 5))
            cax = ax.imshow(dat, aspect='auto', interpolation=None,
                            vmin=-vmax, vmax=vmax,cmap='RdBu')
            ax.plot(arrival[:, 0], arrival[:, 1], 'r.')
            ax.set_title(f'No. samples to remove: {np.round(np.mean(arrival[:, 1])).astype(int)}')
            ax.set_ylim(400, 0)
            fig.colorbar(cax, ax=ax)
            plt.show()

    points = np.asarray(points)
    timezero = find_timezero(points[:,0], points[:,1])
    if plot:
        # plot the mean of the time zero's
        plt.figure()
        colors = ['r', 'b', 'g', 'm', 'k']
        [plt.plot(trace, color=colors[index]) for index, trace in enumerate(traces)]
        [plt.vlines(s, min(traces[index]), max(traces[index]), color=colors[index], linestyle='--')
         for index, s in enumerate(samples)]
        plt.vlines(timezero, np.min(np.min(traces)), np.max(np.max(traces)), 'k', linestyles='--')
        plt.show()
    return timezero


def find_sample_above_threshold(trace: np.array,
                                threshold: float,
                                normalize: bool = True,
                                absolute_val: bool = True):
    """Take in a trace, (optionally) normalize and returh the sample at which a threshold is reached
    The threshold is sign dependent (optionally)
    Args:
        trace: a 1D numpy array
        threshold: the value to look for
        normalize: Whether to normalize the trace or not
        absolute_val: whether to look for both positive and negative intersections
    Returns:
        the crossing of the trace with the threshold"""
    if normalize:
        trace /= np.max(np.abs(trace))
    if absolute_val:
        trace = np.abs(trace)
    return np.argmin(trace < threshold)

def find_timezero(distances, samples):
    """Fit a polynomial to the distances and samples, and then give the intersection of the line

    Args:
        distances:
        samples:

    Returns:
        time_zero

    """
    P = np.polyfit(distances, samples, deg=1)
    # return np.round(P[1]).astype(int)
    # TODO: Wouldn't the intersection be the zero coefficient?
    return np.round(P[0]).astype(float)
    # print('Speed of light is ', (1/P[1]),' cm per nanosecond')

def get_time_vector(sfreq: float,
                    data_in: NDArray[Shape["* traces, * shape"], Float],
                    sample_time_zero: int
                    ):
    """
    Create an equally spaced time vector
    Args:
        sfreq: Sampling frequency
        data_in: Data matrix
        samples_remove: Samples to remove. See "time_zero_correction" function
    Returns:
        time_vector
    """
    time_window = data_in.shape[0] / sfreq
    travel_time = np.linspace(0, time_window, data_in.shape[0])
    travel_time -= travel_time[sample_time_zero]
    return travel_time