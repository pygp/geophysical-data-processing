import numpy as np
from scipy.fft import fft, ifft, fftfreq
from scipy.interpolate import CubicSpline

def taper(y, dt, perc_taper=5):
    """
    Taper a trace
    Args:
        y: trace data 1-dimensional
        dt: Sampling rate
        perc_taper: Tapper percentage (normally between 2-5%)
    Returns:
        y: tappered signal

    """
    tmp1 = len(y)

    perc_taper = perc_taper / 100
    n_taper = int(tmp1 * perc_taper)
    t1 = dt * np.arange(0, n_taper, 1)

    taper_func1 = np.sin(np.pi/2 * 1/t1[n_taper-1] * t1)**2
    taper_func2 = np.flip(taper_func1)

    taper_func = np.concatenate([taper_func1, np.ones((1, tmp1-2*n_taper)), taper_func2], axis=None)
    return y*taper_func


def pad(y, dt, N, fc):
    """
    Zero padding following Boore(2005)
    Args:
        y: Trace 1-D
        dt: Sampling rate
        N: degree
        fc: Corner frequency i.e. Highpass frequency
    Returns:
        y: Padded trace
    """
    Tpad = 1.5*N/fc  # duration of the strip of zeroes
    Tpad = (np.ceil(Tpad/2)*2).astype(int)  # use only even number of seconds
    Npad = np.round(Tpad/dt)  # length of the vector of zeroes
    Ntot_pad = (len(y) + Npad).astype(int)  # updated length after padding
    a_pad = np.zeros(Ntot_pad)  # vector of zeroes
    a_pad[np.round(Npad/2).astype(int): (len(y) + np.round(Npad/2)).astype(int)] = y
    return a_pad


def fft_trace(x, dt):
    """
    Calculate the fourier transform of a trace using the fft
    Args:
        x: trace in 1-D
        dt: sampling interval
    Returns:
        frequency: from 0 to nyquist frequency
        y: frequency spectrum with real and complex components

    """
    # let's use a number of points that is a power of two for efficiency purposes
    nfft = 2 ** np.ceil(np.log2(len(x))).astype(int)
    y = fft(x, nfft) * dt
    f = fftfreq(nfft, dt)
    return f[:nfft//2], y[:nfft//2]


def ifft_trace(y,):
    """
    Calculates the inverse of the fast fourier transform.
    The imaginary component is not returned
    Args:
        y:

    Returns:

    """
    x = ifft(y).real
    return x

def trim_traces(data: np.ndarray, time: float, sample_rate: float):
    """
    Trim traces and or trace according to a time.
    Args:
        data: Numpy array containing the info of all traces (samples x traces)
        delay: time to trim in seconds from the beginning of the recording
        sample_rate: Sampling rate

    Returns:
        trimmed_data

    """
    return data[int(np.abs(time) // sample_rate):]

def stack_traces(data: np.ndarray or list, stack_type: str or tuple='linear'):
    """
    Taken from obspy/signal/util.py -> stack

    Stack tensor. If only one trace then 2D array is used to stack

    Args:
        data: (to stack x samples x traces) if single traces then (to stack x samples)
        stack_type: Type of stack, one of the following:
        ``'linear'``: average stack (default),
        ``('pw', order)``: phase weighted stack of given order
                (see [Schimmel1997]_, order 0 corresponds to linear stack),
        ``('root', order)``: root stack of given order
                (order 1 corresponds to linear stack).
    Returns:
    """
    if isinstance(data, list):
        shape = data[0].shape
        for i, dat in enumerate(data):
            if dat.shape != shape:
                print('Array have not the same amount of samples. Forcing same size. '
                      '\nCheck the input arrays: '
                      '\nFile with shape {} reshaped to {}'.format(dat.shape, shape))
                _dat = np.zeros(shape)
                _dat[:dat.shape[0], :dat.shape[1]] = dat
                data[i] = _dat
        data = np.asarray(data)

    if stack_type == 'linear':
        stack = np.mean(data, axis=0)
    elif stack_type[0] == 'pw':
        from scipy.signal import hilbert
        from scipy.fftpack import next_fast_len
        npts = np.shape(data)[1]
        nfft = next_fast_len(npts)
        anal_sig = hilbert(data, N=nfft)[:, :npts]
        norm_anal_sig = anal_sig / np.abs(anal_sig)
        phase_stack = np.abs(np.mean(norm_anal_sig, axis=0)) ** stack_type[1]
        stack = np.mean(data, axis=0) * phase_stack
    elif stack_type[0] == 'root':
        r = np.mean(np.sign(data) * np.abs(data)
                    ** (1 / stack_type[1]), axis=0)
        stack = np.sign(r) * np.abs(r) ** stack_type[1]
    else:
        raise ValueError('stack type is not valid.')
    return stack

def interpolate_clipping(data,
                         downscale_factor: float = 0.95):
    """
    A function that interpolates clipping over saturated traces.
    It loops over all traces (columns) and defines max and min values
    to replace them by the interpolated values. Default interpolation is spline.

    Args:
        data: (to stack x samples x traces) if single traces then (to stack x samples)
        downscale_factor: Factor to downscale the max and min values
    Returns:
        data: Interpolated data with the same shape as the input data
        mask: Boolean array with the same shape as the input data, True values are the interpolated values
    """
    data = data.copy()
    traces = data.shape[1]
    # normalize data column-wise
    normalization = np.max(np.abs(data), axis=0)
    mask = np.zeros_like(data, dtype=bool)
    for trace_number in range(traces):
        trace = data[:, trace_number]/normalization[trace_number]
        temp = np.logical_or(trace >= np.max(trace) * downscale_factor,
                             trace <= np.min(trace) * downscale_factor)
        temp[np.isnan(trace)] = True
        mask[temp, trace_number] = True
        # check if there are enough non-clipped samples
        if np.sum(~temp) > 10:
            samples = np.arange(len(trace))
            interp_fun = CubicSpline(samples[~temp],
                                     trace[~temp])
            data[temp, trace_number] = interp_fun(samples[temp])*normalization[trace_number]
    return data, mask
