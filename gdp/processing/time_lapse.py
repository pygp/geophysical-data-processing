import numpy as np
from scipy.signal import hilbert, convolve2d


def hilbert_phase_difference(data1,
                             data2,
                             filt_z: int = 1,
                             filt_x: int = 1,
                             rotate: bool = True):
    """Compute the phase difference between two signals using the Hilbert transform.
    Arguments:
        data1: 1D array
        data2: 1D array
        filt_z: The number of rows in the filter.
        filt_x: The number of columns in the filter.
        rotate: Whether to rotate the filter by 180 degrees.
    Returns:
        The phase difference between the two signals.
    """

    if rotate:
        H = np.rot90(np.ones((filt_z, filt_x)))
    else:
        H = np.ones((filt_z, filt_x))
    product = hilbert(data1, axis=0) * np.conj(hilbert(data2, axis=0))
    conv = convolve2d(product, H, mode='valid')
    # Compute the phase difference using convolve
    return np.angle(conv)
