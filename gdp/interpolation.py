import numpy as np
import scipy.interpolate
import skimage.transform

def interpolate_to_new_size(arr: np.ndarray, size: (int, int), **kwargs):
    """
    2D cubic interpolation to up-sample or down-sample the input matrix array. [samples, traces]
    Args:
        arr: (M x N) matrix; [samples, traces]
        size: New shape
    Returns:
        Modified shape of arr
    """
    return skimage.transform.resize(arr,
                                    size,
                                    order=3,
                                    mode='edge',
                                    anti_aliasing=True,
                                    preserve_range=False,
                                    **kwargs)


def interpolate_missing_traces(array: np.ndarray, method='cubic'):
    """
    A function that interpolates missing traces in the input matrix Din. Cubic interpolation is used
    Args:
        array: A matrix of time series where the nearby traces are assumed to be continuous.
            Some traces in this matrix are assumed to be missing and filled with zero's. [samples, traces]
        method: ['nearest', 'linear', 'cubic']

    Returns:
        A matrix where missing traces are interpolated [samples, traces]
    """
    arr = array.copy()
    arr[np.isnan(arr)] = 0
    missing_traces, = np.where(np.sum(arr, axis=0) == 0)
    samples, traces = np.arange(arr.shape[0]), np.arange(arr.shape[1])

    print('Missing traces: {}/{} - Percentage is {} %'.format(len(missing_traces),
                                                              len(traces),
                                                              100 * len(missing_traces) / len(traces)))
    if len(missing_traces) == 0:
        return arr
    traces = np.setdiff1d(traces, missing_traces)

    # make the meshes
    X, Y = np.meshgrid(samples, traces)
    Xq, Yq = np.meshgrid(samples, missing_traces)

    arr[:, missing_traces] = scipy.interpolate.griddata((X.T.ravel(), Y.T.ravel()),
                                                        arr[:, traces].ravel(),
                                                        (Xq.T.ravel(), Yq.T.ravel()),
                                                        method=method).reshape((len(samples), len(missing_traces)))

    return arr

def interpolate_trajectory(trajectory_distance: np.array = None,
                           trajectory: np.array = None,
                           interpolation_distance: np.array = None) -> np.array:
    """
    Interpolate a 3D trajectory over a new distance vector. The trajectory is defined in XYZ and
    an additional trajectory distance vector. Linear interpolation is done along the new distance vector.
    Args:
        trajectory_distance: n by 1 numpy array with the distance along the trajectory
        trajectory: n by 3 numpy array with the trajectory points
        interpolation_distance: m by 1 vector with the new distance to interpolate over
    Returns:
        m by 3 interpolated trajectory
    """
    int_trajectory = np.zeros((interpolation_distance.shape[0], 3))
    int_trajectory[:, 0] = np.interp(interpolation_distance, trajectory_distance, trajectory[:, 0])
    int_trajectory[:, 1] = np.interp(interpolation_distance, trajectory_distance, trajectory[:, 1])
    int_trajectory[:, 2] = np.interp(interpolation_distance, trajectory_distance, trajectory[:, 2])
    return int_trajectory

