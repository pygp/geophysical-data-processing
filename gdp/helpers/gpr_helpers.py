import pandas as pd
import numpy as np
from typing import Union
import os

def read_GPR_spreadsheet(directory: str,
                         gpr_filename: str,
                         antenna_table_filepath: str):
    """Read in an excel spreadsheet with GPR metadata and return a dictionary with the information
    Args:
        directory: directory where the file is located
        filename: name of the file
        antenna_table_filepath: where to read the antenna information from
    Returns:
        dictionary with the information
    """
    # check if file exists
    if not os.path.isfile(directory + gpr_filename):
        raise FileNotFoundError("File not found. Check that the file exists and try again")
    spreadsheet = pd.read_excel(directory + gpr_filename, index_col=0, nrows=30, sheet_name='Fieldnotes')
    antenna_table = pd.read_excel(antenna_table_filepath, index_col=None, nrows=16)
    header = {}
    # get the row from index given the index string
    header['Short description'] = spreadsheet.loc['Short description'][0]
    header['Participants'] = spreadsheet.loc['Participants'][0]
    header['Date'] = spreadsheet.loc['Date'][0]
    header['Time'] = spreadsheet.loc['Time'][0]
    header['Borehole name'] = spreadsheet.loc['Name'][0]
    header['Location'] = spreadsheet.loc['Location (TM)'][0]
    header['Length (m)'] = spreadsheet.loc['Length (m)'][0]
    header['Diameter (mm)'] = spreadsheet.loc['Diameter (mm)'][0]
    header['Water level (m)'] = spreadsheet.loc['Water level (m)'][0]
    header['Casing'] = spreadsheet.loc['Casing (yes/no)'][0]
    header['Antenna model'] = spreadsheet.loc['Antenna model'][0]
    header['Frequency (MHz)'] = spreadsheet.loc['Frequency (MHz)'][0]
    header['Rx-Tx separation (m)'] = spreadsheet.loc['Rx-Tx separation (m)'][0]
    header['Zero to head (m)'] = spreadsheet.loc['Cable zero marker to antenna top (m)'][0]
    header['Shift (m)'] = spreadsheet.loc['Additional length shift (m)'][0]

    # find antenna midpoint from top
    index = np.multiply(
        np.multiply(np.array(antenna_table['Nominal frequency (MHz)'] == header['Frequency (MHz)']),
                    np.array(antenna_table['Separator (m)'] == header['Rx-Tx separation (m)'])),
        np.array(header['Antenna model'] == antenna_table['Antenna model']))
    # raise error if index is empty
    if not np.any(index):
        raise ValueError('Antenna information not found in the antenna table')
    header['Head to midpoint (m)'] = antenna_table['Midpoint from antenna top (m)'].loc[index].to_numpy()[0]
    print(header['Head to midpoint (m)'])

    # Find line number where the entry occurs
    line_number = np.argmax(spreadsheet.index.isin(['Separation (cm)'])) + 1
    time_zero = pd.read_excel(directory + gpr_filename, skiprows=line_number, nrows=5)
    profiles = pd.read_excel(directory + gpr_filename, skiprows=line_number + 7, nrows=10)

    output = {}
    output['fieldnotes'] = pd.DataFrame(header, index=[0])
    output['time zero'] = time_zero
    output['profiles'] = profiles
    return output


def extract_depth_vector(fieldnotes_df,
                         how_many_traces:int,
                         profile_header: dict,
                         start_marker: Union[int, float],
                         end_marker: Union[int, float]
                         ) -> np.array:
    zero_to_head = fieldnotes_df['Zero to head (m)']
    head_to_midpoint = fieldnotes_df['Head to midpoint (m)']
    offset = zero_to_head + head_to_midpoint
    trace_spacing = profile_header['distance_interval']
    depth_from_system = trace_spacing * how_many_traces
    depth_from_markers = np.abs(start_marker - end_marker)
    print('Profile length from system is ' + str(depth_from_system))
    print('Profile length from markers is ' + str(depth_from_markers))
    if np.isnan(depth_from_markers):
        depth_vector = np.linspace(depth_from_system, 0, how_many_traces) + offset.to_numpy()[0]
    else:
        depth_vector = np.linspace(depth_from_markers, 0, how_many_traces) + offset.to_numpy()[0]
    return depth_vector


def apply_standard_processing_SHGPR(data_matrix,
                                    header: pd.DataFrame,
                                    fieldnotes: pd.DataFrame,
                                    tz_sample: Union[float, int] = None):
    from gdp.processing.filtering import remove_mean, horizontal_filter, filter_data
    from gdp.processing.general import interpolate_clipping
    from gdp.processing.image_processing import remove_svd
    from gdp.processing.gain import apply_gain
    frequency_GHz = header['frequency'] / 1000
    profile = {'raw data': data_matrix}
    profile['TW travel time (ns)'] = np.linspace(0, header['time_window'], data_matrix.shape[0])
    if tz_sample is not None:
        tz_time = tz_sample / frequency_GHz  # time zero in nanoseconds
        profile['TW travel time (ns)'] = profile['TW travel time (ns)'] - tz_time
    profile['no mean data'], _ = remove_mean(profile['raw data'], 700, 928)
    profile['no clip data'], _ = interpolate_clipping(profile['no mean data'],
                                                      downscale_factor=0.99)
    print('Frequency in GHz is ' + str(np.round(frequency_GHz)))
    if fieldnotes['Frequency (MHz)'][0] == 100:
        frequency_filt = [0.01, 0.1]
        print('Filtering for 100 MHz antennas')
    elif fieldnotes['Frequency (MHz)'][0] == 250:
        frequency_filt = [0.01, 0.35]
        print('Filtering for 250 MHz antennas')
    else:
        frequency_filt = [0.01, 0.5]
        print('Filtering for wide range (1 to 500 MHz) antennas')
    profile['bandpass data'] = filter_data(profile['no clip data'],
                                           fq=frequency_filt,
                                           sfreq=frequency_GHz,
                                           btype='bandpass')
    profile['svd data'], profile['removed part from svd filter'] = remove_svd(profile['bandpass data'],
                                                                              low_s=0, high_s=3)
    profile['horizontal filter data'] = horizontal_filter(profile['bandpass data'], 500)
    profile['removed part from horizontal filter'] = profile['horizontal filter data'] - profile[
        'bandpass data']
    exponent = 1.5
    profile['gain data after svd'], profile['gain matrix'] = apply_gain(profile['svd data'],
                                                                        sfreq=frequency_GHz,
                                                                        gain_type='linear',
                                                                        exponent=exponent)
    profile['gain data after median'], gain_matrix2 = apply_gain(profile['horizontal filter data'],
                                                                 sfreq=frequency_GHz,
                                                                 gain_type='linear', exponent=exponent)
    profile['removed part from horizontal filter'], gain_matrix2 = apply_gain(
        profile['removed part from horizontal filter'], sfreq=frequency_GHz,
        gain_type='linear', exponent=exponent)
    return profile
