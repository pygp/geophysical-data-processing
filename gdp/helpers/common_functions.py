import numpy as np
import os

def create_directory(directory: str = None):
    """
    Try to make
    Args:
        directory: Directory to load the file
    Returns:
        creates the directory if it does not exist
    """
    try:
        os.makedirs(directory, exist_ok=False)
    except FileExistsError:
        print('Output directory exists')


def convert_str_to_int(m):
    if isinstance(m, int):
        return m
    elif isinstance(m, np.int64):
        return int(m)
    elif isinstance(m, float):
        if np.isnan(m):
            return -1
        else:
            return int(m)
    elif isinstance(m, str):
        return int(m.split('m')[0])  # get rid of m of meters
    else:
        print('{} can not be converted to int. \nType {}'.format(m, type(m)))
        raise AttributeError

def return_numpy_array(array_in):
    '''Check if it is an xarray or pandas df or numpy array and return numpy array'''
    if isinstance(array_in, np.ndarray):
        return array_in
    else:
        return array_in.to_numpy()

def get_antenna_dimensions(antenna_frequency='100MHz'):
    """

    Args:
        antenna_frequency:

    Returns:

    """
    if antenna_frequency == '100MHz':
        Tx_length = 1.7  # [m] Total length of the transmitter antenna
        Rx_length = 1.9  # [m] Total length of the receiver antenna

        total_length_antenna_connected = 4.41
        midpoint_from_antenna_top = 2.283
        sr_offset = 2.77
    elif antenna_frequency == '250MHz':

        Tx_length = 1.31  # [m] Total length of the transmitter antenna
        Rx_length = 1.34  # [m] Total length of the receiver antenna

        middle_length = 0.66  # [m] Separation between bottom of receiver antenna and top of receiver antenna

        sr_offset = (Tx_length * 0.5) + middle_length + (
                    Rx_length * 0.5)  # [m] Distances between transmitter and receiver (exact sensor position)
        total_length_antenna_connected = Tx_length + middle_length + Rx_length  # Total length of antennas in borehole
        midpoint_from_antenna_top = total_length_antenna_connected * 0.5
    elif antenna_frequency == '500MHz':
        # Tx and Rx: Is the same antenna length. Use any
        Tx_length = 0.86
        Rx_length = 0.86
        sr_offset = 0
        total_length_antenna_connected = 0.86
        midpoint_from_antenna_top = Tx_length * 0.5
    elif antenna_frequency in ['1000MHz', '1GHz']:
        # Tx and Rx: Is the same antenna length. Use any
        Tx_length = 0.655
        Rx_length = 0.655
        sr_offset = 0.156
        total_length_antenna_connected = 0.655
        midpoint_from_antenna_top = Tx_length * 0.5
    else:
        raise AttributeError('Antenna frequency not recognized. Use ["100MHz", "250MHz","500MHz","1000MHz"]')
    return Tx_length, Rx_length, total_length_antenna_connected, midpoint_from_antenna_top, sr_offset