"""
Created on 22/10/2021
@authors: Alexis Shakas, Daniel Escallon
"""

import os

__version__ = '1.0'
PACKAGE_DIR = os.path.dirname(__file__) + os.sep
TEST_DIR = os.path.abspath(PACKAGE_DIR + '../test/test_data') + os.sep
DATA_DIR = os.path.abspath(PACKAGE_DIR + '../data') + os.sep
OUTPUT_DIR = os.path.abspath(PACKAGE_DIR + '../output') + os.sep
USER_DIR = os.path.abspath(PACKAGE_DIR + '../user') + os.sep

if not os.path.isdir(DATA_DIR):
    os.mkdir(DATA_DIR)

if not os.path.isdir(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)

if not os.path.isdir(USER_DIR):
    os.mkdir(USER_DIR)
