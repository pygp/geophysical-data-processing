import matplotlib.pyplot as plt
import numpy as np
from matplotlib.gridspec import GridSpec
from gdp.interpolation import interpolate_trajectory
import pyvista as pv
from typing import Union

def plot_trace_time(trace, dt: float, **kwargs):
    """
    1D numpy array containing the
    Args:
        trace:
        dt: sampling rate
    Returns:

    """
    # time vector
    t = np.linspace(0, len(trace) * dt, len(trace), endpoint=True)

    fig, ax = plt.subplots()
    ax.plot(t, trace, **kwargs)
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Amplitude (-)')

    return fig


def plot_trace_freq(trace, dt: float, **kwargs):
    """
    1D numpy array containing the
    Args:
        trace:
        dt: sampling rate
    Returns:

    """
    from gdp.processing import (detrend, filter_data, taper, pad, fft_trace)

    y = detrend(trace)
    y = taper(y, dt, 5)  # 5% tapering
    y = detrend(y)
    f, y = fft_trace(y, dt)

    fig, ax = plt.subplots()
    ax.plot(f, np.abs(y), **kwargs)
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Fourier amplitude spectra (-)')
    fig.show()
    return fig


def plot_frequency_information(trace, dt, **kwargs):
    """
    Spectrum Representations
    The plots show different spectrum representations of a signal.
    A (frequency) spectrum of a discrete-time signal is calculated by
    utilizing the fast Fourier transform (FFT).
    Args:
        trace:
        dt:

    Returns:

    """
    t = np.linspace(0, len(trace) * dt, len(trace), endpoint=True)

    fig = plt.figure(constrained_layout=True, **kwargs)
    gs = GridSpec(3, 2, figure=fig)
    # plot time signal:
    ax1 = fig.add_subplot(gs[0, :])
    ax2 = fig.add_subplot(gs[1, 0])
    ax3 = fig.add_subplot(gs[1, 1])
    ax4 = fig.add_subplot(gs[2, 0])
    ax5 = fig.add_subplot(gs[2, 1])

    ax1.set_title("Signal")
    ax1.plot(t, trace, color='C0')
    ax1.set_xlabel("Time (s)")
    ax1.set_ylabel("Amplitude")

    # plot different spectrum types:
    ax2.set_title("Magnitude Spectrum")
    ax2.magnitude_spectrum(trace, Fs=1 / dt, color='C1')

    ax3.set_title("Log. Magnitude Spectrum")
    ax3.magnitude_spectrum(trace, Fs=1 / dt, scale='dB', color='C1')

    ax4.set_title("Phase Spectrum ")
    ax4.phase_spectrum(trace, Fs=1 / dt, color='C2')

    ax5.set_title("Angle Spectrum")
    ax5.angle_spectrum(trace, Fs=1 / dt, color='C2')

    fig.show()
    return fig


def drape_2D_data_in_3D(traces: np.array,
                        samples: np.array,
                        data: np.array,
                        drape_trajectory: np.array = None,
                        scalar_name: str = "GPR Amplitude (-)",
                        sample_skip: list = None,
                        cross_vector: np.array = np.array([0, 0, 1])) -> pv.StructuredGrid:
    """

    Args:
        traces: A 1D array with the depth associated with each trace
        samples: A 1D array with the trace position along the line
        data: A 2D array with the amplitudes associated with the GPR signal
        drape_trajectory: An n by 3 array with the trajectory over which the data is draped
        scalar_name: Which name to give to the PolyData grid so that amplitudes can be accessed
        sample_skip: A list with two integers defining how many samples to skip along the distance and radius respectively
        cross_vector: A vector that defines, along with the drape trajectory, the direction at which the data is draped
    Returns:
        3D structured pyvista grid of the draped data surface
    """
    if sample_skip is not None:
        traces = traces[::sample_skip[0]]
        samples = samples[::sample_skip[1]]
        data = data[::sample_skip[0], ::sample_skip[1]]
    ntraces = traces.shape[0]
    nsamples = samples.shape[0]
    parallels = drape_trajectory[:-1, :] - drape_trajectory[1:, :]
    parallels = parallels / np.repeat(np.linalg.norm(parallels, axis=1), 3).reshape(-1, 3)
    parallels = np.append(parallels, np.array([parallels[-1, :]]), axis=0)
    normals = np.cross(parallels, cross_vector)

    X = np.repeat(drape_trajectory[:, 0], nsamples).reshape((ntraces, nsamples))
    Y = np.repeat(drape_trajectory[:, 1], nsamples).reshape((ntraces, nsamples))
    Z = np.repeat(drape_trajectory[:, 2], nsamples).reshape((ntraces, nsamples))

    plane_matrix = np.outer(normals, samples).reshape(normals.shape[0], 3, -1)

    grid = pv.StructuredGrid(X + plane_matrix[:, 0, :],
                             Y + plane_matrix[:, 1, :],
                             Z + plane_matrix[:, 2, :])
    grid[scalar_name] = data.ravel(order="F")
    return grid

def plot_image_comparison(im_before,
                          im_after,
                          removed,
                          autoscale: bool = False,
                          fig_kwargs: dict = {},
                          kwargs: dict = {}):
    '''Take in an image that has been filtered and show the before, after and filtered part
    in 3 separate rows that share the same extent, axis labels and colorbar.
    Args:
        im_before: image before filtering
        im_after: image after filtering
        removed: image after filtering
        extend: extend of the image
    Returns:
        fig: figure with the 3 images
        ax: axes of the figure
        '''
    fig, ax = plt.subplots(3, 1, sharex=True, sharey=True, **fig_kwargs)
    im1 = ax[0].imshow(im_before, cmap='seismic', **kwargs)
    ax[0].set_title('Before', loc='left')
    ax[0].set_ylabel('Samples (-)')
    ax[0].set_xlabel('Traces (-)')
    im2 = ax[1].imshow(im_after, cmap='seismic', **kwargs)
    ax[1].set_title('After', loc='left')
    ax[1].set_ylabel('Samples (-)')
    ax[1].set_xlabel('Traces (-)')
    im3 = ax[2].imshow(removed, cmap='seismic', **kwargs)
    ax[2].set_title('Removed', loc='left')
    ax[2].set_ylabel('Samples (-)')
    ax[2].set_xlabel('Traces (-)')
    if autoscale:
        datasets = [im_before, im_after, removed]
        # autoscale each subplot to the maximum of the dataset being plotted
        for index, im in enumerate([im1, im2, im3]):
            # retrieve the data from the image handle
            data = datasets[index]
            max_val = np.mean(np.abs(data.ravel()))
            im.set_clim(-max_val, max_val)
    plt.show()
    return fig, ax


def plot_trace_comparison(im_before, im_after, trace, kwargs: dict = {}):
    '''Take in an image that has been filtered and show the before, after and filtered part
    in 3 separate rows that share the same extent, axis labels and colorbar.
    Args:
        im_before: image before filtering
        im_after: image after filtering
        removed: image after filtering
        extend: extend of the image
    Returns:
        fig: figure with the 3 images
        ax: axes of the figure
        '''
    fig, ax = plt.subplots(1,1)
    ax.plot(im_before[:, trace], label='Before')
    ax.plot(im_after[:, trace], linestyle='--', label='After')
    ax.set_title('Before - after comparison', loc='left')
    ax.set_xlabel('Samples (-)')
    # apply matplotlib kwargs
    ax.set(**kwargs)
    plt.legend()
    plt.show()
    return fig, ax

def plot_profile_with_labels(profile, time_vector, depth_vector):
    minmax = np.max(np.abs(profile.ravel()))
    plot_kwargs = {'extent': [np.min(time_vector),
              np.max(depth_vector),
              np.max(time_vector),
              np.min(depth_vector)],
                   'vmin': -minmax,
                   'vmax': minmax}
    fig, axs = plot_profile(profile, kwargs=plot_kwargs)
    axs.set_xlabel('Depth (m)')
    axs.set_ylabel('Travel time (ns)')
    axs.set_aspect('auto')
    return fig, axs

def plot_profile(profile,
                 autoscale: bool = False,
                 kwargs: dict = {}):
    '''Plot a profile in 2D
    Args:
        profile: data to be plotted
        extend: extend of the image
    Returns:
        fig: figure with the 3 images
        ax: axes of the figure
        '''
    fig, ax = plt.subplots(1, 1)
    im = ax.imshow(profile, cmap='seismic', **kwargs)
    if autoscale:
        max_val = np.mean(np.abs(profile.ravel()))
        im.set_clim(-max_val, max_val)
    return fig, ax


def plot_rectangle(coordinates: Union[list, tuple, np.array],
                   axs: plt.axes,
                   kwargs: dict = {}):
    """
    Plots a rectangle given its 4 coordinates.

    Args:
        coordinates (list of tuples): A list of 4 (x, y) tuples representing the vertices of the rectangle.
        axs (matplotlib.axes.Axes): The axes on which to plot the rectangle.
    Returns:
        axs (matplotlib.axes.Axes): The axes on which the rectangle was plotted.
    """
    # Ensure we have exactly 4 coordinates
    if len(coordinates) != 4:
        raise ValueError("Exactly 4 coordinates are required to plot a rectangle.")

    # Separate x and y values
    x, y = zip(*coordinates)

    # Close the rectangle by adding the first point at the end
    x = list(x) + [x[0]]
    y = list(y) + [y[0]]
    axs.plot(x, y, **kwargs)
    return axs

def plot_wiggle(traces, scale=1.0, extent=None, ax=None, fill=True, normalize_to_value=None, **kwargs):
    """
    Plot seismic traces in a wiggle plot.

    Parameters
    ----------
    traces : ndarray
        2D array (nsamples x ntraces)
    scale : float
        Scaling factor for traces.
    extent : list or tuple
        (xmin, xmax, ymin, ymax)
    ax : Matplotlib Axes
        Axes to plot on. If None, a new figure is created.
    fill: Bool
        Fill the wiggle
    normalize_to_value: float
        Normalize the traces to a value. If None, the traces are normalized to the maximum value.
    """
    nsamples, ntraces = traces.shape
    if extent is None:
        offsets = np.arange(ntraces)
        t = np.arange(nsamples)
    else:
        offsets = np.linspace(np.min(extent[:2]), np.max(extent[:2]), ntraces)
        t = np.linspace(np.min(extent[2:]), np.max(extent[2:]), nsamples)

    if ax is None:
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots()

    if normalize_to_value is None:
        normalize_to_value = np.abs(traces).max()
    traces = traces.copy() / normalize_to_value  # Normalize
    color = kwargs.pop('color', 'black')
    for offset, tr in zip(offsets, traces.T):
        x = offset + tr * scale
        ax.plot(x, t, color=color, **kwargs)
        if 'label' in kwargs:
            kwargs.pop('label')
        if fill:
            ax.fill_betweenx(t, offset, x, where=(x > offset), color=color)
    return ax
