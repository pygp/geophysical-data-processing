import requests
from gdp import DATA_DIR
from pathlib import Path
import os

def download_data_from_url(url: str,
                           filename:str,
                           target_folder: str = DATA_DIR):
    """
    Download dataset from url using requests
    Args:
        url: url for the download
        filename: Name of the file to save
        target_folder: Output folder
    Returns:
        None
    """
    # Check if the file is already downloaded
    if Path(os.path.join(target_folder, filename)).exists():
        print(f'Data from {url} already downloaded and saved as {filename}')
        return
    r = requests.get(url, allow_redirects=True)
    with open(os.path.join(target_folder, filename), 'wb') as f:
        f.write(r.content)
    print(f'Data from {url} downloaded and saved as {filename}')