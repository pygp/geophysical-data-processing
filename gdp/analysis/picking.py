import numpy as np
import cmapy


def transform_coordinates(x: int, y: int, shape: tuple or list, extent: tuple or list):
    """
    Transform the coordinates to the given extent.
    Args:
        x: pixel x
        y: pixel y
        shape: shape of th image
        extent: (xmin, xmax, ymin, ymax)

    Returns:

    """
    # return (x * (extent[1] - extent[0]) / shape[1]) + extent[0], (y * (extent[3] - extent[2]) / shape[0]) + extent[2]
    # using cv2 imshow, the y axis coordinates are inverted
    return (x * (extent[1] - extent[0]) / shape[1]) + extent[0], (y * (extent[2] - extent[3]) / shape[0]) + extent[3]


@staticmethod
def compute_crossing(pick: dict,
                     zero_crossing: float = 1):
    crossing = pick['true coordinates'][
        pick['true coordinates'][:, 1] < zero_crossing, 0]
    if crossing.shape[0] > 0:
        pick['horizontal intercept'] = [np.min(crossing), np.max(crossing)]
        print('intercept saved as output for pick')
    crossing = pick['true coordinates'][
        pick['true coordinates'][:, 0] < zero_crossing, 1]
    if crossing.shape[0] > 0:
        pick['vertical intercept'] = [np.min(crossing), np.max(crossing)]
        print('intercept saved as output for pick')
    return pick


cache = None
img_org = None
name_line = 'line_0'
hpath = {name_line: []}
clim = 1
step = 0.1


def pick_line(image: np.ndarray,
              extent: list or tuple = None,
              colormap: str = None,
              interpolate_path: bool = False,
              automatic_naming: bool = False,
              compute_lengths: bool = False,
              compute_zero_crossing: float = None,
              values_along_path: bool = False,
              **kwargs) -> dict:
    """
    Draw a path on the given image and return the path (pixels) and the transformed path according to the given extent.

    Args:
        image: numpy array of the image
        extent: (xmin, xmax, ymin, ymax)
        colormap: Return a colored image with matplotlib colormap (e.g., cividis or seismic)
        interpolate_path: interpolate to all pixel values between picks
        automatic_naming: whether to prompt the user for naming or use "Line (iteration)"
        compute_lengths: Whether to compute the length of the picked lines. Requires extent
        compute_zero_crossing: Given a minumum value, a zero crossing will be computed for each pick which is where
            the pick gets close to a coordinate axis
        values_along_path: interpolate and export the values of the image along the chosen paths
        **kwargs:
            title: title of the window
            cmap:

    Returns:
        dictionary with pixel picks and (optionally) coordinates of picks / lengths / zero crossings
    """
    global cache, name_line, hpath, clim, img_org
    import cv2
    import copy

    if values_along_path:
        from scipy.interpolate import RectBivariateSpline
        if extent[0] > extent[1]:
            temp_ext = extent
            extent[0] = temp_ext[1]
            extent[1] = temp_ext[0]
            image = np.flip(image, axis=1)
        if extent[2] > extent[3]:
            temp_ext = extent
            extent[2] = temp_ext[3]
            extent[3] = temp_ext[2]
            image = np.flip(image, axis=0)
        x = np.linspace(extent[0], extent[1], image.shape[1])
        y = np.linspace(extent[2], extent[3], image.shape[0])
        f = RectBivariateSpline(x, y, np.flip(image.T, axis=1))
    if automatic_naming:
        name_line = 'pick_0'
    else:
        name_line = input('Enter name of line:')
    if name_line == '':
        name_line = 'pick_0'
    hpath = {name_line: []}
    print(f'New line: {name_line}')
    cache = None
    img_org = None

    title = kwargs.pop('title',
                       'Left Mouse (pick) | Middle Mouse (new line) | Right Mouse (del) | C/Esc/Q (quit) | W/S (cmap up, down)')
    radius_point = kwargs.pop('radius_point', 10)
    thickness_line = int(radius_point * 0.5)
    if thickness_line == 0: thickness_line = 1
    cv2.namedWindow(title, cv2.WINDOW_GUI_NORMAL)
    try:
        cv2.namedWindow(title, cv2.WINDOW_GUI_NORMAL)
    except cv2.error:
        pass
    cv2.resizeWindow(title, 900, 900)

    img_org = copy.deepcopy(image)

    def get_colored(img_org,
                    colormap: str = colormap,
                    scale: float = 1):
        cmap = cmapy.cmap(colormap)
        norm = img_org / np.max(np.abs(img_org))
        norm = norm - norm.min()
        norm /= norm.max()
        norm -= 0.5
        norm *= scale
        norm += 0.5
        scaled = norm * 255
        scaled[scaled > 255] = 255
        scaled[scaled < 0] = 0
        # return cv2.applyColorMap(scaled.astype(np.uint8), cv2.COLORMAP_RAINBOW)
        return cv2.applyColorMap(scaled.astype(np.uint8), cmap)

    if colormap:
        image = get_colored(image, colormap=colormap)
    cache = copy.deepcopy(image)
    shape = image.shape

    def draw_path(event, x, y, flags, param):
        global name_line, hpath
        if event == cv2.EVENT_LBUTTONDBLCLK:
            print(x, y)
            hpath[name_line].append([x, y])
        elif event == cv2.EVENT_RBUTTONDBLCLK:
            a = hpath[name_line].pop(-1)
            print('delete ', a)
            global cache
            cache = None
            cache = copy.deepcopy(image)
            cv2.imshow(title, cache)
        elif event == cv2.EVENT_MBUTTONDBLCLK:
            # n = name_line.split('_')
            # name_line = n[0] +'_'+ str(int(n[1]) + 1)
            a = True
            counter = 1
            while a:
                if automatic_naming:
                    n = 'pick_' + str(counter)
                    counter += 1
                else:
                    n = input('Enter name of line and hit Enter:')
                if n == '':
                    n = 'pick_' + str(len(hpath))
                if n not in list(hpath.keys()):
                    name_line = n
                    a = False
                else:
                    print(f'"{n}" already exists')
            hpath[name_line] = []
            print(f'New line: {name_line}')

    cv2.setMouseCallback(title, draw_path)  # , param=name_line)

    while True:
        cv2.imshow(title, cache)
        for namel, lpoint in hpath.items():
            if len(lpoint) > 0:
                for point in lpoint:
                    cv2.circle(cache, point, radius_point, (225, 0, 0), -1)
                if len(lpoint) > 1:
                    for count in range(len(lpoint)):
                        if count == 0:
                            continue
                        cv2.line(cache, hpath.get(namel)[count - 1], hpath.get(namel)[count], (225, 0, 0), thickness_line)
        key = cv2.waitKey(1)

        if (key == ord("c") or
                key == 27 or
                key == ord("q")):
            break

        if key == ord("w"):  # Upkey
            clim += step
            image = get_colored(img_org, scale=clim)
            cache = copy.deepcopy(image)
            print(clim)
        if key == ord("s"):  # Downkey
            clim -= step
            if clim <= 0:
                clim = step
            image = get_colored(img_org, scale=clim)
            cache = copy.deepcopy(image)
            print(clim)

    cv2.destroyAllWindows()

    if interpolate_path is True:
        for key, lpoints in hpath.items():
            interpolated_path = np.empty((0, 2))
            for index in range(len(lpoints) - 1):
                # first go over the horizontal pixels (traces)
                points = np.abs(lpoints[index][0] - lpoints[index + 1][0]) + 1
                new_path = np.zeros((points, 2))
                new_path[:, 0] = np.linspace(lpoints[index][0],
                                             lpoints[index + 1][0],
                                             points)
                new_path[:, 1] = np.round(np.linspace(lpoints[index][1],
                                                      lpoints[index + 1][1],
                                                      points))
                interpolated_path = np.concatenate((interpolated_path, new_path), axis=0)
            hpath[key] = {'pixel coordinates': interpolated_path.astype(int)}
    else:
        for key, lpoints in hpath.items():
            hpath[key] = {'pixel coordinates': np.asarray(lpoints).astype(int)}

    if extent is not None:
        for key, item in hpath.items():
            lpoints = item['pixel coordinates']
            hpath[key]['true coordinates'] = np.asarray(
                [transform_coordinates(x, y, shape=shape, extent=extent) for x, y in lpoints])
            print('true coordinates saved as output for pick')
            if hpath[key]['true coordinates'].any():
                if compute_lengths:
                    l = 0
                    for index in range(hpath[key]['true coordinates'].shape[0] - 1):
                        l += np.sqrt((hpath[key]['true coordinates'][index, 0] - hpath[key]['true coordinates'][
                            index + 1, 0]) ** 2 +
                                     (hpath[key]['true coordinates'][index, 1] - hpath[key]['true coordinates'][
                                         index + 1, 1]) ** 2)
                    hpath[key]['length'] = l
                    print('length saved as output for pick')

                if compute_zero_crossing is not None:
                    hpath[key] = compute_crossing(hpath[key])
            if values_along_path:
                hpath[key]['path values'] = f(hpath[key]['true coordinates'][:, 0],
                                              hpath[key]['true coordinates'][:, 1],
                                              grid=False)
                path_distance = np.cumsum([np.linalg.norm(hpath[key]['true coordinates'][k, :] -
                                                          hpath[key]['true coordinates'][k + 1, :])
                                           for k in range(hpath[key]['true coordinates'].shape[0] - 1)])
                hpath[key]['path distance'] = np.concatenate((np.array([0]), path_distance))

    return hpath


def pick_roi(image: np.ndarray,
                      extent: list or tuple = None,
                      colormap: str = 'Greys',
                      automatic_naming: bool = True,
                      save_image_crop: bool = True,
                      **kwargs) -> dict:
    """
    Draw one or several regions of interest (ROI) on the given image and return the ROI corners in pixels and
    true coordinates, as well as the cropped part of the data.

    Args:
        image: numpy array of the image
        extent: (xmin, xmax, ymin, ymax)
        colormap: Return a colored image with matplotlib colormap (e.g., cividis or seismic)
        automatic_naming: whether to prompt the user for naming or use "Line (iteration)"
        save_image_crop: whether to also save the selected ROI as a dictionary entry
        **kwargs:
            title: title of the window
            cmap:

    Returns:
        dictionary with selected ROI's
    """
    # TODO: Use same exit keys as in pick_line
    global cache, name, hpath, clim, img_org, counter
    import cv2
    import copy
    if automatic_naming:
        name = 'roi_0'
    else:
        name = input('Enter name of roi:')
    if name == '':
        name = 'roi_0'
    hpath = {name: []}
    print(f'New roi: {name}')
    cache = None
    img_org = None
    title = kwargs.pop("title",
                       "Left Mouse (top left corner) | Right Mouse (bottom right corner) | Middle Mouse (save ROI) |\n R (delete last ROI) | C (quit and save) | W/S (scale colormap up/down)")
    image[np.isnan(image)] = 0
    raw_data = np.copy(image)
    img_org = copy.deepcopy(image)
    cv2.namedWindow(title, cv2.WINDOW_GUI_NORMAL)
    cv2.resizeWindow(title, 900, 900)

    def get_colored(img_org,
                    colormap: str,
                    scale: float = 1):
        cmap = cmapy.cmap(colormap)
        norm = img_org / np.max(np.abs(img_org))
        norm = norm - norm.min()
        norm /= norm.max()
        norm -= 0.5
        norm *= scale
        norm += 0.5
        scaled = norm * 255
        scaled[scaled > 255] = 255
        scaled[scaled < 0] = 0
        return cv2.applyColorMap(scaled.astype(np.uint8), cmap)

    start = None
    end = None
    counter = 0
    hpath = {}
    if colormap:
        image = get_colored(image, colormap=colormap)
    cache = copy.deepcopy(image)
    shape = image.shape

    # Define the callback function for the mouse events
    def select_rect(event, x, y, flags, param):
        global start, end, counter, name
        # If the left mouse button was clicked, record the starting coordinates
        if event == cv2.EVENT_LBUTTONDBLCLK:
            start = (x, y)
            cv2.circle(cache, start, 10, (225, 0, 0), -1)
        elif event == cv2.EVENT_RBUTTONDBLCLK:
            end = (x, y)
            cv2.circle(cache, end, 10, (0, 0, 255), -1)
        elif event == cv2.EVENT_MBUTTONDBLCLK:
            hpath[name] = {'pixel coordinates': [start[0], start[1], end[0], end[1]]}
            x = [start[0], end[0]]
            y = [start[1], end[1]]
            if extent is not None:
                lpoints = [(x[0], y[0]), (x[1], y[0]), (x[1], y[1]), (x[0], y[1])]
                coordinates = [transform_coordinates(x, y, shape=shape, extent=extent) for x, y in lpoints]
                hpath[name]['true coordinates'] = coordinates
            if save_image_crop:
                hpath[name]['data'] = raw_data[int(min(y)):int(max(y)),
                                      int(min(x)):int(max(x))]

            a = True
            while a:
                if automatic_naming:
                    counter += 1
                    n = 'roi_' + str(counter)
                else:
                    n = input('Enter name of roi and hit Enter:')
                if n == '':
                    n = 'roi_' + str(len(hpath))
                if n not in list(hpath.keys()):
                    name = n
                    a = False
                else:
                    print(f'"{n}" already exists')
            start = None
            end = None
            print_rectangles()

    # Set the mouse callback function for the window
    cv2.setMouseCallback(title, select_rect)

    def print_rectangles():
        try:
            for key, values in hpath.items():
                cv2.rectangle(cache, (values['pixel coordinates'][0],
                                      values['pixel coordinates'][1]),
                              (values['pixel coordinates'][2],
                               values['pixel coordinates'][3]), (0, 255, 0), 2)
                middle = (
                values['pixel coordinates'][0] + (values['pixel coordinates'][2] - values['pixel coordinates'][0]) / 2,
                values['pixel coordinates'][1] + (values['pixel coordinates'][3] - values['pixel coordinates'][1]) / 2)
                cv2.putText(cache, key, (int(middle[0]),
                                         int(middle[1])),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
        except TypeError:
            pass

    while True:
        cv2.imshow(title, cache)
        key = cv2.waitKey(1)
        cv2.setMouseCallback(title, select_rect)
        if key == ord("c"):
            break
        # change cmap limits
        elif key == ord("w"):
            clim += step
            image = get_colored(img_org,
                                colormap=colormap,
                                scale=clim)
            cache = copy.deepcopy(image)
            print_rectangles()
        elif key == ord("s"):
            clim -= step
            if clim <= 0:
                clim = step
            image = get_colored(img_org,
                                colormap=colormap,
                                scale=clim)
            cache = copy.deepcopy(image)
            print_rectangles()

        elif key == ord("r"):
            cache = image.copy()
            remove_key = list(hpath.keys())[-1]
            hpath.pop(remove_key, None)
            print_rectangles()
        elif key == ord("n"):
            cv2.setMouseCallback(title, select_rect)

    cv2.destroyAllWindows()
    return hpath


def _pick_image(img: np.ndarray, extent: list or tuple = None, **kwargs):
    """
    Draw a path on the given image and return the path (pixels) and the transformed path according to the given extent.

    Args:
        img: numpy array of the image
        extent: (xmin, xmax, ymin, ymax), if no extents, it will just return the path  pixel coordinates
        **kwargs:
            title: title of the window

    Returns:

    """

    from bokeh.plotting import figure, output_file, show, Row
    from bokeh.models import DataTable, TableColumn, PointDrawTool, ColumnDataSource
    import pandas as pd

    df = pd.DataFrame(columns=('x', 'y', 'model x', 'model y'))
    source = ColumnDataSource(data=df)

    title = kwargs.pop('title', 'Pick a path')
    cmap = kwargs.pop('cmap', 'Viridis256')
    # file to save the model
    output_file("pick_image.html")
    tools = "pan,wheel_zoom,box_zoom,reset,hover,crosshair"
    p = figure(title=title,
               tools=tools,
               tooltips=[("x", "$x"), ("y", "$y"), ("value", "@image")])
    p.x_range.range_padding = p.y_range.range_padding = 0

    shape = img.shape[:2]
    if len(img.shape) == 3:  # RGB image
        image = np.empty(shape, dtype=np.uint32)
        view = image.view(dtype=np.uint8).reshape(shape + (4,))
        view[:, :, :3] = img[:, :, :3]
        view[:, :, -1] = 255
        rgb = True

    elif len(img.shape) == 4:  # RGBA image
        image = np.empty(shape, dtype=np.uint32)
        view = image.view(dtype=np.uint8).reshape(shape + (4,))
        view[:, :, :] = img[:, :, :]
        rgb = True

    elif len(img.shape) == 2:  # grayscale image
        rgb = False
        image = img.copy()

    if rgb:
        p.image_rgba(image=[image], x=0, y=0, dw=shape[1], dh=shape[0])
    else:
        p.image(image=[image], x=0, y=0, dw=shape[1], dh=shape[0],
                palette=cmap, level="image")

    ren1 = p.scatter(x='x', y='y', source=source, color='red', size=10)
    ren2 = p.line(x='x', y='y', source=source, color='red')
    columns = [TableColumn(field="x", title="x"),
               TableColumn(field="y", title="y"),
               TableColumn(field='model x', title='model x'),
               TableColumn(field='model y', title='model y')]
    table = DataTable(source=source, columns=columns, editable=True, height=500)
    draw_tool = PointDrawTool(renderers=[ren1], )
    p.add_tools(draw_tool)
    p.toolbar.active_tap = draw_tool

    def my_callback(attr, old, new):
        print(new)

    source.on_change("data", my_callback)
    show(Row(p, table))
    return source


def _pick_first_arrival(img: np.ndarray, threshold: float = 0.5):
    """
    Pick the first arrival point of the image.
    Args:
        img: numpy array of the image. Columns are traces and Rows are the samples
        threshold: threshold of the image
        **kwargs:
            title: title of the window

    Returns:
        x, y: the first arrival point of the image
    """
    samples, traces = img.shape
    # d = np.diff(img.copy(), axis=0)
    d = img.copy()
    _t = np.abs(d).max(axis=0)
    d = np.divide(d, _t, where=_t != 0)
    pick = []
    for trace in range(traces):
        for sample in range(samples - 1):
            if np.abs(d[sample, trace]) > threshold:
                pick.append((trace, sample))
                break
    return np.asarray(pick)


def pick_first_arrival(img: np.ndarray,
                       threshold: float = 0.15):
    """
    Pick the first arrival point of the image, based on the cumulative energy of the trace
    Args:
        img: numpy array of the image. Columns are traces and Rows are the samples
        threshold: threshold of the image
        **kwargs:
            title: title of the window

    Returns:
        x, y: the first arrival point of the image
    """
    samples, traces = img.shape
    # d = np.diff(img.copy(), axis=0)
    d = np.abs(img.copy())
    # d = np.divide(d, _t, where=_t != 0)

    cd = np.cumsum(d, axis=0)
    _t = cd[-1]
    cd_n = np.divide(cd, _t, where=_t != 0)
    # create a nan array
    pick = []
    for trace in range(traces):
        for sample in range(samples - 1):
            if cd_n[sample, trace] > threshold:
                pick.append((trace, sample))
                break
    return np.asarray(pick)

# TODO: Check this picking tool
def AIC_picker(time_vector, amplitude_vector):
    AICmin = []  # AIC(minimum) of the loaded files calculated with the AIC picker
    t0 = []  # all time values of the loaded files at the AIC(minimum) calculated with the AIC picker
    ampl0 = []  # all amplitde values of the loaded files at the AIC(minimum) calculated with the AIC picker
    fAIC = []  # nested list of all results of the AIC-function (fAIC) for all files
    nsamp = len(amplitude_vector)
    AIC = np.zeros(
        len(amplitude_vector) + 1)  # creates array with length = (number of elements of Y), filled with zeros for each in Y

    for index, ampl in enumerate(amplitude_vector):  # each amplitude in Y[specific file = i] gets index starting with 0

        k = index + 1  # since enumerate() starts with 0 -> for math k needs to be +1

        AIC[index] = k * np.log(np.var(amplitude_vector[0:index])) + (nsamp - k - 1) * (
            np.log(np.var(amplitude_vector[k:nsamp - 1])))
        '''
        ############ formula to get no error ###############
        # var(..) of lists with only 1 element, 0, inf-, or NaN-values cause error
        #  starts to calculate with index > 1 and < 996 (1 < index < 996)
            AIC[index] = k*np.log(np.var(Y[i][0:index]))+(nsamp - k - 1)*(np.log(np.var(Y[i][k: nsamp-1])))
        '''

        ### Code gives error due to some values dividing by 0, calculating with NaN, -inf
        # replace all -inf, inf with Nan
        # np.nan_to_num(AIC, copy=False, nan=0.0, neginf=0.0, posinf = 0.0)         # replaces (cope=False) all NaN, -inf, inf with zero
        '''
        ###### check how many AIC values == 0 ####### and what's there index ####################
        for index, value in enumerate(AIC):
            if value ==0:
                print(index)
        print(len(AIC))
        AICnew = np.delete(AIC, np.where(AIC == 0.0))
        print(len(AICnew))

        ###### check how many AIC values == 0 ####### and what's there index ####################
        for index, value in enumerate(AIC):
            if value ==0:
                print(index)
        print(len(AIC))
        AICnew = np.delete(AIC, np.where(AIC == 0.0))
        print(len(AICnew))
        '''

    for i in AIC:
        if i == -np.inf:
            i = np.nan

    # finds index of minimum value of AIC-function after AIC calculated for each possible value of Y[i]
    np.nan_to_num(AIC, copy=False, nan=0.0, neginf=0.0,
                  posinf=0.0)  # replaces (cope=False) all NaN, -inf, inf with zero
    # AIC[np.isnan(AIC)] = np.nan[np.isnan(AIC)] = np.nan                             # Claudio's version
    # AIC[AIC == -np.inf] = np.nan                                                    # Claudio's version

    # min_index = np.argmin(AIC)              # np.argmin = Returns the indices of the minimum values along an axis
    min_index = np.argmax(AIC == np.min(AIC))
    AICmin.append(np.amin(
        AIC))  # np.amin() = Return the minimum of an array or minimum along an axis, here appends minimum AIC-value to list: AICmin
    # np.nanamin = Return minimum value of an array along a given axis, ignoring any NaNs ->
    # AIC-array includs still -inf, nan's
    t0.append(time_vector[min_index])  # appends corresponding time to list
    ampl0.append(amplitude_vector[min_index])  # appends corresponding amplitude to list

    fAIC.append(AIC)  # saves AIC-array for each Y[i] = file in nested list
    return t0, ampl0, fAIC