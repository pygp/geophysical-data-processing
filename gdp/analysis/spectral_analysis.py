import numpy as np


def fourier_transform(trace, dt: float):
    """
    Perform the fourier transform to an specific trace, to extract its frequency information.

    Args:
        trace:
        dt: sampling rate

    Returns:

    """
    fN = 1 / (2 * dt)  # Nyquist freq
    nfft = 2 ** np.ceil(np.log2(len(trace)))

    duration = nfft * dt