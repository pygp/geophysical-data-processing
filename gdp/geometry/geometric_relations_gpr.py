import numpy as np
from scipy import interpolate
from nptyping import NDArray, Shape, Float


def get_radial_distance(sr_offset: float,
                        velocity: float,
                        time_vector: NDArray[Shape['* samples'], Float]):
    """
    Function that computes (geometrically) the travel-time vector from the
    antenna midpoint, the Radius and zero depth (for borehole antennas)
    Preset transmitter-receiver offset
    transmitter on bottom, receiver on top

    Calculate radial distance based on travel time (pythagoras)
    Radial distance from antenna borehole (using a constant velocity model)
    velocity in meters per nanosecond

    Args:
        sr_offset: source-receiver offset
        velocity: velocity of medium
        time_vector: Time vector (After time zero correction)


    Returns:
        radial_distance: Real radial distance
    """
    # time_window = data_in.shape[0] / sfreq
    # travel_time = np.linspace(0, time_window, data_in.shape[0])
    travel_time = time_vector.copy()
    travel_time[travel_time<0] = 0
    radial_distance = np.real(np.sqrt((velocity * travel_time/2) ** 2 - (sr_offset / 2) ** 2))
    # Check the values below 0 will be
    index = np.isnan(radial_distance).sum()
    # time_zero = travel_time[index]  # This is the real time 0 when the signal arrives
    # time_vector -= time_zero  # Better this way so we don't modify the initial data
    spacing = radial_distance[-1] - radial_distance[-2]
    filling = np.linspace(-index*spacing, radial_distance[index]-spacing, index)
    # index_vector = radial_distance > 0
    # data_out = data_in[index_vector]
    radial_distance[np.isnan(radial_distance)] = filling
    # travel_time = travel_time[index_vector]
    return radial_distance


def correct_depth_for_markers(data_in,
                              traces,
                              depths,
                              ):
    """
    If measured in time. Markers are obtained every spacing value.
    This will correct the traces for an accurate spacing
    Args:
        data: Raw data
        traces: Trace at which marker was measured.
            traces[0]: start of measuring - depth 0
            traces[-1]: end of measurement - last depth position-stop
        depths: Corresponds to the borehole depth at which it was measured
    Returns:
        data: Traces not used are deleted
        borehole_depths: Vector indicating the position of the trace in the borehole
    """
    x = np.arange(traces[0], traces[-1] + 1)
    xp = traces
    fp = depths
    borehole_depth = np.interp(x, xp, fp)
    data_temp = data_in[:, traces[0]:traces[-1] + 1]


    # xx, yy = np.meshgrid(borehole_depth, np.arange(0,data.shape[0]))

    # z = np.sin(xx ** 2 + yy ** 2)
    samples = np.arange(0, data_temp.shape[0])

    new_bor_depth = np.linspace(borehole_depth[0], borehole_depth[-1], len(borehole_depth))

    # f = interpolate.interp2d(borehole_depth, samples, data_temp,
    #                          kind='nearest')
    xx, yy = np.meshgrid(borehole_depth, samples)


    new_xx, new_yy = np.meshgrid(new_bor_depth, samples)

    grid = interpolate.griddata(np.c_[xx.flatten(), yy.flatten()],
                                data_temp.flatten(),
                                (new_xx, new_yy),
                                method='linear')


    return data_in[:,traces[0]:traces[-1]+1], borehole_depth
