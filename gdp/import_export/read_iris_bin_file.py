# Written by J. GANCE
# V3.0 - 04/11/2019
# Python code to read IRIS Instruments .bin and .pro files
import struct
from tkinter import filedialog
import datetime

def read_file():
    # Select the .bin or .pro file
    filename = filedialog.askopenfilename(filetypes=[('BIN Files', '*.bin'), ('PRO Files', '*.pro')], title='File '
                                                                                                            'selector')
    if filename == '':
        exit()
    else:
        file = filename
        fid = open(file, 'rb')
        fid.seek(0, 2)
        TailleTotale = fid.tell()
        fid.seek(0, 0)
        if filename[-3:] == 'bin':
            data = {}
            data['version'] = struct.unpack('I', fid.read(4))[0]
            data['TypeOfSyscal'] = struct.unpack('B', fid.read(1))[0]
            if (data['TypeOfSyscal'] == 8 or data['TypeOfSyscal'] == 9 or data['TypeOfSyscal'] == 3 or data['TypeOfSyscal'] == 11 or data['TypeOfSyscal'] == 4 or data['TypeOfSyscal'] == 5 or data['TypeOfSyscal'] == 10) or (data['version'] >= 2147483650 and (data['TypeOfSyscal'] == 2 or data['TypeOfSyscal'] == 7 or data['TypeOfSyscal'] == 1 or data['TypeOfSyscal'] == 6)):
                data['comment'] = fid.read(1024).decode('utf-8').strip('\x00')
            if ((data['TypeOfSyscal'] == 8 or data['TypeOfSyscal'] == 9 or data['TypeOfSyscal'] == 3 or data['TypeOfSyscal'] == 11 or data['TypeOfSyscal'] == 4 or data['TypeOfSyscal'] == 5) and data['version'] == 2147483651) or ((data['TypeOfSyscal'] == 1 or data['TypeOfSyscal'] == 6 or data['TypeOfSyscal'] == 10) and data['version'] >= 2147483651):
                data['ColeCole'] = []
                for _ in range(64000):
                    data['ColeCole'].append(struct.unpack('3f', fid.read(12)))
            if data['version'] >= 2147483652:
                data['CommonFilePath'] = fid.read(260).decode('utf-8').strip('\x00')
                data['NbFiles'] = struct.unpack('H', fid.read(2))[0]
                data['SizeFileName'] = []
                data['FileNameIabOrVmn'] = []
                for _ in range(data['NbFiles']):
                    data['SizeFileName'].append(struct.unpack('H', fid.read(2))[0])
                    data['FileNameIabOrVmn'].append(fid.read(data['SizeFileName'][-1]).decode('utf-8').strip('\x00'))
            if data['TypeOfSyscal'] == 8 or data['TypeOfSyscal'] == 9 or data['TypeOfSyscal'] == 3 or data['TypeOfSyscal'] == 11 or data['TypeOfSyscal'] == 4 or data['TypeOfSyscal'] == 5:
                Position = fid.tell()
                i = 1
                while Position < TailleTotale:
                    # Reading electrode array
                    data['Measure'][i]['el_array'] = struct.unpack('h', fid.read(2))[0]
                    # Reading ???
                    data['Measure'][i]['MoreTMesure'] = struct.unpack('h', fid.read(2))[0]
                    # Reading Injection time
                    data['Measure'][i]['time'] = struct.unpack('f', fid.read(4))[0]
                    # Reading M_delay
                    data['Measure'][i]['m_dly'] = struct.unpack('f', fid.read(4))[0]
                    # Reading Kid or not
                    data['Measure'][i]['TypeCpXyz'] = struct.unpack('h', fid.read(2))[0]
                    if data['Measure'][i]['TypeCpXyz'] == 0:
                        print('These data have been recorded with the Syscal KID and can\'t be read')
                    # Reading ignored parameter
                    data['Measure'][i]['Q'] = struct.unpack('h', fid.read(2))[0]
                    # Reading electrode positions
                    data['Measure'][i]['pos'] = struct.unpack('12f', fid.read(48))
                    # Reading PS
                    data['Measure'][i]['Ps'] = struct.unpack('f', fid.read(4))[0]
                    # Reading Vp
                    data['Measure'][i]['Vp'] = struct.unpack('f', fid.read(4))[0]
                    # Reading In
                    data['Measure'][i]['In'] = struct.unpack('f', fid.read(4))[0]
                    # Reading resistivity
                    data['Measure'][i]['rho'] = struct.unpack('f', fid.read(4))[0]
                    # Reading chargeability
                    data['Measure'][i]['m'] = struct.unpack('f', fid.read(4))[0]
                    # Reading Q
                    data['Measure'][i]['dev'] = struct.unpack('f', fid.read(4))[0]
                    # Reading IP Windows duration (Tm)
                    data['Measure'][i]['Tm'] = struct.unpack('20f', fid.read(80))
                    # Reading IP Windows values
                    data['Measure'][i]['Mx'] = struct.unpack('20f', fid.read(80))
                    data['Measure'][i]['Channel'] = struct.unpack('B', fid.read(1))[0]
                    data['Measure'][i]['NbChannel'] = struct.unpack('B', fid.read(1))[0]
                    data['Measure'][i]['Overload'] = struct.unpack('?', fid.read(1))[0]
                    data['Measure'][i]['ChannelValide'] = struct.unpack('?', fid.read(1))[0]
                    data['Measure'][i]['unused'] = struct.unpack('B', fid.read(1))[0]
                    data['Measure'][i]['QuadNumber'] = struct.unpack('H', fid.read(2))[0]
                    data['Measure'][i]['Name'] = fid.read(12).decode('utf-8').strip('\x00')
                    data['Measure'][i]['Latitude'] = struct.unpack('f', fid.read(4))[0]
                    data['Measure'][i]['Longitude'] = struct.unpack('f', fid.read(4))[0]
                    data['Measure'][i]['NbCren'] = struct.unpack('f', fid.read(4))[0]
                    data['Measure'][i]['RsChk'] = struct.unpack('f', fid.read(4))[0]
                    if data['Measure'][i]['MoreTMesure'] == 2:
                        data['Measure'][i]['TxVab'] = struct.unpack('f', fid.read(4))[0]
                        data['Measure'][i]['TxBat'] = struct.unpack('f', fid.read(4))[0]
                        data['Measure'][i]['RxBat'] = struct.unpack('f', fid.read(4))[0]
                        data['Measure'][i]['Temperature'] = struct.unpack('f', fid.read(4))[0]
                    elif data['Measure'][i]['MoreTMesure'] == 3:
                        data['Measure'][i]['TxVab'] = struct.unpack('f', fid.read(4))[0]
                        data['Measure'][i]['TxBat'] = struct.unpack('f', fid.read(4))[0]
                        data['Measure'][i]['RxBat'] = struct.unpack('f', fid.read(4))[0]
                        data['Measure'][i]['Temperature'] = struct.unpack('f', fid.read(4))[0]
                        data['Measure'][i]['DateTime'] = struct.unpack('d', fid.read(8))[0]
                        data['Measure'][i]['DateTime'] = datetime.datetime.fromordinal(int(data['Measure'][i]['DateTime'])) + datetime.timedelta(days=data['Measure'][i]['DateTime'] % 1) - datetime.timedelta(days=366)
                    if data['version'] >= 2147483652:
                        data['Measure'][i]['Iabfile'] = struct.unpack('h', fid.read(2))[0]
                        data['Measure'][i]['Vmnfile'] = struct.unpack('h', fid.read(2))[0]
                    Position = fid.tell()
                    i += 1
                fid.close()
            else:
                print('Device not managed for the moment')
        elif filename[-3:] == 'pro':
            data = {}
            data['download_version'] = struct.unpack('i', fid.read(4))[0]
            data['size'] = struct.unpack('i', fid.read(4))[0]
            page = -1
            i = 0
            while True:
                i += 1
                page = struct.unpack('i', fid.read(4))[0]
                test = struct.unpack('I', fid.read(4))[0]
                if not test:
                    break
                data['Measure'][i]['date'] = test
                data['Measure'][i]['Name'] = fid.read(12).decode('utf-8').strip('\x00')
                data['Measure'][i]['Channel'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['NbChannel'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['Overload'] = struct.unpack('?', fid.read(1))[0]
                data['Measure'][i]['ChannelValide'] = struct.unpack('?', fid.read(1))[0]
                data['Measure'][i]['unused'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['QuadNumber'] = struct.unpack('H', fid.read(2))[0]
                data['Measure'][i]['vrunning'] = struct.unpack('?', fid.read(1))[0]
                data['Measure'][i]['vsigned'] = struct.unpack('?', fid.read(1))[0]
                data['Measure'][i]['normalized'] = struct.unpack('?', fid.read(1))[0]
                data['Measure'][i]['imperial'] = struct.unpack('?', fid.read(1))[0]
                data['Measure'][i]['unused'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['timeSet'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['timeMode'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['type'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['unused2'] = struct.unpack('B', fid.read(1))[0]
                data['Measure'][i]['el_array'] = struct.unpack('i', fid.read(4))[0]
                data['Measure'][i]['time'] = struct.unpack('H', fid.read(2))[0]
                data['Measure'][i]['vdly'] = struct.unpack('H', fid.read(2))[0]
                data['Measure'][i]['mdly'] = struct.unpack('H', fid.read(2))[0]
                data['Measure'][i]['tm'] = struct.unpack('20H', fid.read(40))
                data['Measure'][i]['unused3'] = struct.unpack('H', fid.read(2))[0]
                data['Measure'][i]['Latitude'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['Longitude'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['inrx'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['pos'] = struct.unpack('12f', fid.read(48))
                data['Measure'][i]['mov'] = struct.unpack('3f', fid.read(12))
                data['Measure'][i]['rho'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['dev'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['NbCren'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['RsChk'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['TxVab'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['TxBat'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['RxBat'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['Temperature'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['Ps'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['Vp'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['In'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['m'] = struct.unpack('f', fid.read(4))[0]
                data['Measure'][i]['Mx'] = struct.unpack('20f', fid.read(80))
                rcrc = struct.unpack('I', fid.read(4))[0]
                if page == -1:
                    exit()
        else:
            exit()
    return data