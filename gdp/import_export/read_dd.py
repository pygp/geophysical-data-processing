"""
File created by Hansruedi Maurer, modified by Hagen Soeding and translated by Daniel Escallon
"""
import numpy as np
from tqdm.autonotebook import tqdm

def read_at(fid, dd: dict, code):
    """
    Read attribute specified by code from fid into dd
    Args:
        fid: _io.BufferedReader from "open(filename, 'rb')
        dd: dictionary to save the data
        code: from dd['code'] get the attribute code
    Returns:
    """
    index, = np.where(dd['atcode'] == code)
    if len(index) == 0:
        print('Attribute {} not found'.format(code))
        return
    index = index[0]

    fid.seek(dd['atpos'][index], 0)

    ty = {
        1: chr,  # char
        2: '<i2',  # int16
        3: '<i4',  # int32
        4: '<f8',  # float
        5: '<f8',  # double
        6: '<u8',  # uint64
        7: '<u4',  # uint32
    }

    return np.frombuffer(fid.read(dd['n'] * dd['atsize'][index]),
                         dtype=ty[dd['attype'][index]])


def dd_attachdata(filename: str, dd: dict):
    """
    Attach data after reading .dd file with read_dd

    Args:
        fname: filename where data is hosted
        dd:

    Returns:
        dd
    """
    with open(filename, 'rb') as f:
        dd['data'] = np.zeros((np.max(dd['nsamp']), dd['n']))

        for j in tqdm(range(dd['n']-1)):
            f.seek(dd['pos'][j], 0)
            dd['data'][:, j] = np.frombuffer(f.read(dd['nsamp'][j] * 8), dtype='<f8')
    return dd


def read_dd(filename: str):
    """

    Args:
        filename:

    Returns:
        dd: dict

    """
    dd = {}
    with open(filename, 'rb') as f:
        dd['n'] = np.frombuffer(f.read(4), dtype='<i4')[0]
        dd['nat'] = np.frombuffer(f.read(4), dtype='<i4')[0]
        dd['atcode'] = np.frombuffer(f.read(dd['nat'] * 4), dtype='<i4')
        dd['atsize'] = np.frombuffer(f.read(dd['nat'] * 4), dtype='<i4')
        dd['attype'] = np.frombuffer(f.read(dd['nat'] * 4), dtype='<i4')
        dd['atpos'] = np.frombuffer(f.read(dd['nat'] * 4), dtype='<i4')

        if 1 in dd['atcode']: dd['sh'] = read_at(f, dd,1 )
        if 2 in dd['atcode']: dd['rh'] = read_at(f, dd,2 )
        if 3 in dd['atcode']: dd['sno'] = read_at(f, dd,3 )
        if 4 in dd['atcode']: dd['rno'] = read_at(f, dd,4 )
        if 5 in dd['atcode']: dd['sx'] = read_at(f, dd,5 )
        if 6 in dd['atcode']: dd['sy'] = read_at(f, dd,6 )
        if 7 in dd['atcode']: dd['sz'] = read_at(f, dd,7 )
        if 8 in dd['atcode']: dd['rx'] = read_at(f, dd,8 )
        if 9 in dd['atcode']: dd['ry'] = read_at(f, dd,9 )
        if 10 in dd['atcode']: dd['rz'] = read_at(f, dd,10)
        if 11 in dd['atcode']: dd['sra'] = read_at(f, dd, 11)
        if 12 in dd['atcode']: dd['ampfac'] = read_at(f, dd, 12)
        if 13 in dd['atcode']: dd['tpest'] = read_at(f, dd, 13)
        if 14 in dd['atcode']: dd['tpcalc'] = read_at(f, dd, 14)
        if 15 in dd['atcode']: dd['tpcorr'] = read_at(f, dd, 15)
        if 16 in dd['atcode']: dd['to'] = read_at(f, dd, 16)
        if 17 in dd['atcode']: dd['tspare'] = read_at(f, dd, 17)
        if 18 in dd['atcode']: dd['use'] = read_at(f, dd, 18)
        if 19 in dd['atcode']: dd['nsamp'] = read_at(f, dd, 19)
        if 20 in dd['atcode']: dd['pos'] = read_at(f, dd, 20)
        if 21 in dd['atcode']: dd['sdep'] = read_at(f, dd, 21)
        if 22 in dd['atcode']: dd['rdep'] = read_at(f, dd, 22)
        if 23 in dd['atcode']: dd['dfid'] = read_at(f, dd, 23)

    return dd