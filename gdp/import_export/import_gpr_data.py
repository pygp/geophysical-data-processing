import os
import struct
import numpy as np
import re
import pandas as pd


def load_mala(file_name: str) -> (np.ndarray, dict):
    """
    Load GPR data from MALA
    Args:
        file_name: File with (.rd3, .rd7 or .RD3) extension
    Returns:
        data matrix as numpy array
        information as dictionary
    """
    extensions = ['rd3', 'RD3', 'rd7']
    # try the different extensions to guess file extension
    def try_file_extension(filepath):
        for ext in extensions:
            if os.path.isfile(filepath + '.' + ext):
                return filepath, ext
            else:
                pass
        return None, None


    split_file = file_name.split('.')
    # guess extension if it does not exist
    if len(split_file) == 1:
        filename, extension = try_file_extension(file_name)
        if filename is None:
            raise FileNotFoundError("File could not be found")
    elif len(split_file) == 2:
        filename = split_file[0]
        extension = split_file[1]
    elif split_file[-1] in ['rd3', 'rd7', 'RD3']:
        filename, extension = file_name.split('.', -1)
    else:
        raise FileNotFoundError("File must be '.rd3', '.rd7', or '.RD3' extension")

    if extension in ['rd3', 'RD3']:
        binaries = np.int16
    else:
        binaries = np.int32

    if not os.path.isfile(filename + '.' + extension):
        raise FileNotFoundError("{} not found. Check that the file exists and try again".format(filename + '.' +  extension))

    if os.path.isfile(filename + '.rad'):
        fpath = os.path.abspath(filename + '.rad')
    elif os.path.isfile(filename + '.RAD'):
        fpath = os.path.abspath(filename + '.RAD')
    else:
        raise FileNotFoundError("{}.rad or {}.RAD not found".format(filename, filename))

    with open(fpath, 'r') as f:
        _h = f.read().split('\n')
    _j = [row.split(':', 1) for row in _h]
    head = dict(_j[:-1])
    info = dict()
    info['samples'] = int(head['SAMPLES'])
    info['frequency (MHz)'] = float(head['FREQUENCY'])
    info['frequency (GHz)'] = info['frequency (MHz)'] / 1000
    info['frequency_steps'] = int(head['FREQUENCY STEPS'])
    info['time_window (ns)'] = float(head['TIMEWINDOW'])
    info['time step (ns)'] = 1/info['frequency (GHz)']
    info['stacks'] = int(head['STACKS'])
    info['stacking_time (ns)'] = float(head['STACKING TIME'])
    info['stop_position'] = float(head['STOP POSITION'])
    info['time_interval'] = float(head['TIME INTERVAL'])
    info['distance_interval (m)'] = float(head['DISTANCE INTERVAL'])
    # Read header file information and store as a dictionary
    data = np.fromfile(filename + '.' + extension, dtype=binaries)

    # Reshape based on sample size
    size, = data.shape
    n_samples = int(info['samples'] * np.floor(size / info['samples']))

    data = np.reshape(data[:n_samples], (int(np.floor(size / info['samples'])), info['samples'])).T
    info['traces'] = data.shape[1]
    info['travel time (ns)'] = np.linspace(0, info['samples'] / info['frequency (GHz)'], info['samples'])

    return data, info

def load_gssi(filename: str) -> (np.ndarray, dict):
    try:
        from readgssi import readgssi
    except ImportError:
        raise ImportError("Please install readgssi package")
    return readgssi.readgssi(filename)

def load_dt1(filename: str) -> (np.ndarray, dict):
    """
    Load GPR data from DT1 format (pulsEkko)
    Args:
        filename: File with .DT1 extension
    Returns:
        data matrix,
        header information

    This function is taken from GPRPy which is further adapted from
    http://www.lucabaradello.it/files/dt1read.m
    """
    data_filename = filename + '.DT1'
    if not os.path.isfile(data_filename):
        raise FileNotFoundError("{} not found. Check that the file exists and try again".format(filename))
    headlen = 32
    with open(data_filename, "rb") as datafile:
        datafile.seek(8, 0)  # 0 is beginning of file
        samples, = struct.unpack('f', datafile.read(4))
        samples = int(samples)
        dimtrace = samples * 2 + 128
        datafile.seek(-dimtrace, 2)  # 2 stands for end of file
        # print(datafile.tell())
        max_traces, = struct.unpack('f', datafile.read(4))
        max_traces = int(max_traces)
        # Initialize matrix
        data = np.zeros((samples, max_traces))
        head = np.zeros((headlen, max_traces))
        # Set the reader to the beginning of the file
        datafile.seek(0, 0)
        for j in range(0, max_traces):
            # Read the header info
            for k in range(0, headlen):
                info, = struct.unpack('f', datafile.read(4))
                head[k, j] = info
            # Now the actual data
            for k in range(0, samples):
                # 2 is the size of short, 'h' is the symbol
                pnt, = struct.unpack('h', datafile.read(2))
                data[k, j] = pnt
            datafile.seek(dimtrace * (j + 1), 0)
    return np.asmatrix(data), read_dt1_header(filename)


def read_dt1_header(filename: str) -> dict:
    """
    Load header from HD format (pulsEkko)
    Args:
        filename: File with .HD extension
    Returns:
        header information

    This function is taken from GPRPy which is further adapted from
    http://www.lucabaradello.it/files/dt1read.m
    """
    info = {}
    filepath = os.path.abspath(filename + '.HD')
    if os.path.isfile(filepath):
        with open(filepath, "r", newline='\n') as datafile:
            datafile.readline().strip()
            info["system"] = datafile.readline().strip()
            info["date"] = datafile.readline().strip()
            string = datafile.readline().strip()
            var = re.match(r'NUMBER OF TRACES   = (.*)', string)
            info["N_traces"] = int(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'NUMBER OF PTS/TRC  = (.*)', string)
            info["N_pts_per_trace"] = int(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'TIMEZERO AT POINT  = (.*)', string)
            info["TZ_at_pt"] = float(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'TOTAL TIME WINDOW  = (.*)', string)
            info["Total_time_window"] = float(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'STARTING POSITION  = (.*)', string)
            info["Start_pos"] = float(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'FINAL POSITION     = (.*)', string)
            info["Final_pos"] = float(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'STEP SIZE USED     = (.*)', string)
            info["Step_size"] = float(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'POSITION UNITS     = (.*)', string)
            info["Pos_units"] = str(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'NOMINAL FREQUENCY  = (.*)', string)
            info["Freq"] = float(var.group(1))
            string = datafile.readline().strip()
            var = re.match(r'ANTENNA SEPARATION = (.*)', string)
            info["Antenna_sep"] = float(var.group(1))
            # If you need more of the header info, you can just continue as above
            # Transform feet to meters
            if info['Pos_units'] == 'ft':
                info["Start_pos"] = info["Start_pos"] * 0.3048
                info["Final_pos"] = info["Final_pos"] * 0.3048
                info["Step_size"] = info["Step_size"] * 0.3048
                info["Antenna_sep"] = info["Antenna_sep"] * 0.3048
                info['Pos_units'] = 'm'
    else:
        raise FileNotFoundError("{}.HD not found".format(filename))
        info = {}
    return info


def read_markers_mala(filename: str, delete_markers: list = []):
    """
    Read the markers from the mala system
    Args:
        filename: .mrk file.
        delete_markers: list containing the markers to delete in case extra ones were put

    Returns:
        data frame containig the trace number of the marker
    """
    import pandas as pd
    markers = pd.read_table(filename,
                            skiprows=1, delimiter='\t', names=['trace no.', 'Version', 'Na'])
    if not isinstance(delete_markers, list):
        delete_markers = [delete_markers]
    for d in delete_markers:
        markers = markers.drop(d).reset_index(drop=True)

    return markers[['trace no.']]


def load_sgy(filename: str,
             path: str = None) -> (np.ndarray, pd.DataFrame):
    """
    Load GPR data from segy files and accompanying csv file as header. This format is found in, for example,
    Proceq GPR devices.
    Args:
        filename: Filename (without the .sgy extension)
        path: Path where the datafiles are found. If not supplied, assumes that path is included in filename
    Returns:
        data matrix as xarray object with dimensions of samples/traces
        header information as pandas dataframe
    """
    import segyio
    import xarray as xr

    if path:
        filename = path + os.sep + filename

    # Check if filename have an extension and if yes remove the extension and keep the root
    if '.' in filename:
        filename = filename.split('.')[0]

    datafile = np.array(segyio.open(filename=filename + '.sgy',
                                    ignore_geometry=True).trace.raw[:].T)
    header = pd.read_csv(filename + '.csv',
                         encoding="utf-16",
                         delimiter='\t',
                         header=None,
                         index_col=0)
    datafile = xr.DataArray(datafile,
                            dims=['samples', 'traces'],
                            )

    return datafile, header
