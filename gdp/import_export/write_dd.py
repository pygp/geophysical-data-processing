"""
File created by Hansruedi Maurer, modified by Hagen Soeding and translated by Daniel Escallon

Write .dd file
----------------
"""
import os
import numpy as np


def write_dd(dd: dict,
             output_filename: str = 'output.dd',
             save_dir: str = os.path.expanduser('~')):
    """

    Args:
        dd: Dictionary containing all 23 attributes
        output_filename: Name of the file
        save_dir: Location to save the file
    Returns:

    """
    output = os.path.join(save_dir, output_filename)
    ntraces = len(dd['data'])
    nat = 23  # Number of attributes
    atcode = list(np.arange(1, nat+1))  # Attribute codes
    # Attribute  sizes
    atsize = np.hstack(
        [4 * np.ones(4),  # int32 [sh, rh, sno, rno]
         8 * np.ones(13),  # double [sx,sy,sz,rx,ry,rz,sra,ampfac,tpest,tpcalc,tpcorr,to,tspare]
         4 * np.ones(2),  # int32 [use,nsamp]
         8 * np.ones(1),  # uint64 [pos]
         8 * np.ones(2),  # double [sdep,rdep]
         4 * np.ones(1),  # uint32 [dfid]
         ]
    ).astype('<i4')
    # Attribute types
    #ty = {1:chr,  # char
    #      2:'<i2',  # int16
    #      3:'<i4',  # int32
    #      4:'<f8',  # float
    #      5:'<f8',  # double
    #      6:'<u8',  # uint64
    #      7:'<u4',  # uint32
    # }
    attype = np.hstack(
        [3*np.ones(4),
         5*np.ones(13),
         3*np.ones(2),
         6*np.ones(1),
         5*np.ones(2),
         7*np.ones(1)],
    ).astype('<i4')

    # set up header - sum1 corresponds to 4 bytes each for number of
    # data_traces, number of attribute and the arrays of attribute codes,
    # attribute sizes, attribute type and attribute position
    # added source and receiver depth as attributes 5/6

    sum1 = 8 + (4 + 4 + 4 + 4) * nat
    atpos = np.zeros(nat)  # Attribute position

    for a in range(nat):
        atpos[a] = sum1
        sum1 += ntraces*atsize[a]

    dummy = np.zeros(ntraces, dtype=np.double).astype('<f8')

    # Write .dd data
    # ----------------
    with open(output, 'wb') as f:
        print('Saving data in file: {}'.format(output_filename))
        # >i4 big endian --- <i4 small endian
        # https://docs.python.org/3/library/struct.html
        # File header information for location of bytes.

        f.write(np.asarray(ntraces, dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray(nat, dtype=np.int32).astype('<i4').tobytes())

        f.write(np.asarray(atcode, dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray(atsize, dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray(attype, dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray(atpos, dtype=np.int32).astype('<i4').tobytes())

        f.write(np.asarray([dd['sh'][val] for val in sorted(dd['sh'].keys())], dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray([dd['rh'][val] for val in sorted(dd['rh'].keys())], dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray([dd['sno'][val] for val in sorted(dd['sno'].keys())], dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray([dd['rno'][val] for val in sorted(dd['rno'].keys())], dtype=np.int32).astype('<i4').tobytes())
        f.write(np.asarray([dd['sx'][val] for val in sorted(dd['sx'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['sy'][val] for val in sorted(dd['sy'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['sz'][val] for val in sorted(dd['sz'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['rx'][val] for val in sorted(dd['rx'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['ry'][val] for val in sorted(dd['ry'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['rz'][val] for val in sorted(dd['rz'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['sra'][val] for val in sorted(dd['sra'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['ampfac'][val] for val in sorted(dd['ampfac'].keys())], dtype=np.double).astype('<f8').tobytes())

        if len(dd.get('tpest')) == 0:
            f.write(dummy.tobytes())
        else:
            f.write(np.asarray([dd['tpest'][val] for val in sorted(dd['tpest'].keys())],
                               dtype=np.double).astype('<f8').tobytes())

        if len(dd.get('tpcalc')) == 0:
            f.write(dummy.tobytes())
        else:
            f.write(np.asarray([dd['tpcalc'][val] for val in sorted(dd['tpcalc'].keys())],
                               dtype=np.double).astype('<f8').tobytes())

        if len(dd.get('tpcorr')) == 0:
            f.write(dummy.tobytes())
        else:
            f.write(np.asarray([dd['tpcorr'][val] for val in sorted(dd['tpcorr'].keys())],
                               dtype=np.double).astype('<f8').tobytes())

        if len(dd.get('to')) == 0:
            f.write(dummy.tobytes())
        else:
            f.write(np.asarray([dd['to'][val] for val in sorted(dd['to'].keys())],
                               dtype=np.double).astype('<f8').tobytes())

        if len(dd.get('tspare')) == 0:
            f.write(dummy.tobytes())
        else:
            f.write(np.asarray([dd['tspare'][val] for val in sorted(dd['tspare'].keys())],
                               dtype=np.double).astype('<f8').tobytes())

        f.write(np.asarray([dd['use'][val] for val in sorted(dd['use'].keys())], dtype=np.int32).astype('<i4').tobytes())
        f.write(
            np.asarray([dd['nsamp'][val] for val in sorted(dd['nsamp'].keys())], dtype=np.int32).astype('<i4').tobytes()
        )

        pospos = f.tell()
        pos = np.zeros(ntraces, dtype=np.uint64).astype('<u8')
        f.write(pos.tobytes())

        # Write additional header values
        f.write(np.asarray([dd['sdep'][val] for val in sorted(dd['sdep'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['rdep'][val] for val in sorted(dd['rdep'].keys())], dtype=np.double).astype('<f8').tobytes())
        f.write(np.asarray([dd['dfid'][val] for val in sorted(dd['dfid'].keys())], dtype=np.uint32).astype('<u4').tobytes())

        # Write trace data
        for a in range(ntraces):
            pos[a] = f.tell()
            f.write(np.asarray(dd['data'][a], dtype=np.float64).astype('<f8').tobytes())

        f.seek(pospos, 0)  # Beginning of file
        f.write(np.asarray(pos, dtype=np.uint64).astype('<u8').tobytes())
    print('Data succesfuly saved in: {}'.format(output))


