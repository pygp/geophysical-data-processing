import numpy as np
import re
import struct
from itertools import groupby

def _all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)

def rgeod3(filename) -> dict:
    """
    read a SEG-2 file from GEODE into Python
    Program of Tom Spillmann modified by Giovanni Piffaretti and translated by
    Daniel Escallon from Matlab to Python
    Args:
        filename:
    Returns:
        eve: dict = keys(['data', 'name', 'ntr', 'nsamp', 'sra', 'delay', 'ACQUISITION_DATE', 'ACQUISITION_TIME'])
    """
    with open(filename, 'rb') as f:
        blockid = hex(int.from_bytes(f.read(2), byteorder='little'))
        if blockid not in '0x3a55':
            raise FileNotFoundError('ERROR rgeod3: not a seismic data file: {}'.format(filename))

        fhead = dict()
        fhead['revn'] = int.from_bytes(f.read(2), byteorder='little')  # Revision number
        fhead['M'] = int.from_bytes(f.read(2), byteorder='little')  # size of trace descriptor block
        N = int.from_bytes(f.read(2), byteorder='little')  # number of traces
        fhead['sterm'] = f.read(1)[0]  # size string termination
        fhead['fstchar'] = f.read(1)[0]  # first string termination
        fhead['sstchar'] = f.read(1)[0]  # second string termination
        fhead['lterm'] = f.read(1)[0]  # size line termination
        fhead['.fltchar'] = f.read(1)[0]  # first line termination
        fhead['sltchar'] = f.read(1)[0]  # second line termination

        # read pointers to traces
        f.seek(32, 0)
        tdb = [int.from_bytes(f.read(4), byteorder='little') for i in range(N)]

        # read file header strings
        # ------------------------------------------

        # read trace header block
        # ------------------------------------------
        f.seek(tdb[0] + 8, 0)  # Data matrix initialize
        nsample = int.from_bytes(f.read(4), byteorder='little')
        nsamp = np.zeros(len(tdb))
        sra = np.zeros(len(tdb))
        delay = np.zeros(len(tdb))

        al_fl = np.zeros(len(tdb))
        amp_recov = np.zeros(len(tdb), dtype=object)
        chan_num = np.zeros(len(tdb), dtype=object)
        desc_fac = np.zeros(len(tdb), dtype=object)
        dig_high_cut = np.zeros(len(tdb), dtype=object)
        dig_low_cut = np.zeros(len(tdb), dtype=object)
        fx_gain = np.zeros(len(tdb), dtype=object)
        line_id = np.zeros(len(tdb), dtype=object)
        low_cut_fl = np.zeros(len(tdb), dtype=object)
        notch_freq = np.zeros(len(tdb), dtype=object)
        raw_record = np.zeros(len(tdb), dtype=object)
        rec_loc = np.zeros(len(tdb), dtype=object)
        sh_seq_num = np.zeros(len(tdb), dtype=object)
        skew = np.zeros(len(tdb), dtype=object)
        sou_loc = np.zeros(len(tdb), dtype=object)
        stack = np.zeros(len(tdb), dtype=object)
        note_tr = np.zeros(len(tdb), dtype=object)

        eve = dict()
        eve['data'] = np.zeros((N, nsample))

        defect = []
        for ti in range(N):  # Number of traces
            fstat = f.seek(tdb[ti], 0)
            trid = hex(int.from_bytes(f.read(2), byteorder='little'))
            # is a trace descriptor block?
            if not (trid in '0x4422'):
                if not (trid in '0x2244'):
                    print(FileNotFoundError('ERROR rgeod3: no trace descriptor block in {} at trace {}. '
                                            'Filling with Nan values.'.format(filename, ti+1)))
                    defect.append(ti)
                    continue

            X = int.from_bytes(f.read(2), byteorder='little')  # size of trace descriptor block
            Y = int.from_bytes(f.read(4), byteorder='little')  # size of following data block

            nsamp[ti] = int.from_bytes(f.read(4), byteorder='little')  # number of samples in data block
            fc = int.from_bytes(f.read(1), byteorder='little')  # format code(4 is 32 bytefloat)
            # modified Giovanni Piffaretti

            # read trace header strings
            # -------------------------
            def _return_inside(val, type=None):
                if val is not None:
                    if type is None:
                        return val.group()
                    elif type=='float':
                        return float(val.group())
                    elif type=='str':
                        return str(val.group())
                    elif type=='int':
                        return int(val.group())
                else:
                    return None

            try:
                f.seek(tdb[ti] + 32, 0)
                b = 1
                sbuf = f.read(432)
                # re.findall(r'(?<=x00)\w+ -?\d+\.?\d*\w?-?\d*', srt(sbuf) # All info
                sra[ti] = _return_inside(re.search(r'(?<=x00SAMPLE_INTERVAL )[-\d.]*', str(sbuf)), type='float')
                delay[ti] = _return_inside(re.search(r'(?<=x00DELAY )[-\d.]*', str(sbuf)), type='float')

                al_fl[ti] = _return_inside(re.search(r'(?<=x00ALIAS_FILTER )[-\d.]*', str(sbuf)), type='float')
                amp_recov[ti] = _return_inside(re.search(r'(?<=x00AMPLITUDE_RECOVERY )[\d\w.]*', str(sbuf)))
                chan_num[ti] = _return_inside(re.search(r'(?<=x00CHANNEL_NUMBER )\d*', str(sbuf)))
                desc_fac[ti] = _return_inside(re.search(r'(?<=x00DESCALING_FACTOR )[\d.\w-]*', str(sbuf)))
                dig_high_cut[ti] = _return_inside(re.search(r'(?<=x00DIGITAL_HIGH_CUT_FILTER )[.\d\w\s]*', str(sbuf)))
                dig_low_cut[ti] = _return_inside(re.search(r'(?<=x00DIGITAL_LOW_CUT_FILTER )[.\d\w\s]*', str(sbuf)))
                fx_gain[ti] = _return_inside(re.search(r'(?<=x00FIXED_GAIN )[.\d\w\s]*', str(sbuf)))
                line_id[ti] = _return_inside(re.search(r'(?<=x00LINE_ID )\d*', str(sbuf)))
                low_cut_fl[ti] = _return_inside(re.search(r'(?<=x00LOW_CUT_FILTER )[.\d\w\s]*', str(sbuf)))
                notch_freq[ti] = _return_inside(re.search(r'(?<=x00NOTCH_FREQUENCY )[-\d.]*', str(sbuf)))
                raw_record[ti] = _return_inside(re.search(r"(?<=x00RAW_RECORD )[><?@+'`~^%&\*\[\]\{\}!#|\\\"$';,:;=/\(\),\-\w\s+]*.[\w\d]*[^\\]", str(sbuf)))
                rec_loc[ti] = _return_inside(re.search(r'(?<=x00RECEIVER_LOCATION )[-\d.]*', str(sbuf)))
                sh_seq_num[ti] = _return_inside(re.search(r'(?<=x00SHOT_SEQUENCE_NUMBER )\d*', str(sbuf)))
                skew[ti] = _return_inside(re.search(r'(?<=x00SKEW )[-\d.]*', str(sbuf)))
                sou_loc[ti] = _return_inside(re.search(r'(?<=x00SOURCE_LOCATION )[-\d.]*', str(sbuf)))
                stack[ti] = _return_inside(re.search(r'(?<=x00STACK )\d*', str(sbuf)))
                nt = _return_inside(re.search(r'\b(x00NOTE ).*\\[^(x)]', str(sbuf)))
                note_tr[ti] = {}
                note_tr[ti]['DISPLAY_SCALE'] = _return_inside(re.search(r'(?<=DISPLAY_SCALE )[\w\d.\s]*', str(nt)))

            except Exception as e:
                raise AttributeError('ERROR rgeod3: could not find trace {} header in {}. \n error message: \n{}'.format(ti+1, filename, e))

            # read data
            # -------------------------
            f.seek(tdb[ti] + X, 0)  # Search at bottoms of file
            eve['data'][ti, :] = [struct.unpack('f', f.read(4))[0] for i in range(int(nsamp[ti]))]

        if len(defect)>0:
            eve['data'][defect, :] = np.nan
            nsamp[defect] = np.nan
            sra[defect] = np.nan
            al_fl[defect] = np.nan
            amp_recov[defect] = np.nan
            chan_num[defect] = np.nan
            desc_fac[defect] = np.nan
            dig_high_cut[defect] = np.nan
            dig_low_cut[defect] =np.nan
            fx_gain[defect] = np.nan
            line_id[defect] = np.nan
            low_cut_fl[defect] = np.nan
            notch_freq[defect] = np.nan
            raw_record[defect] = np.nan
            rec_loc[defect] = np.nan
            sh_seq_num[defect] = np.nan
            skew[defect] = np.nan
            sou_loc[defect] = np.nan
            stack[defect] = np.nan
            note_tr[defect] = np.nan

            eve['missing_traces'] = np.asarray(defect) + 1

        eve['data'] = eve['data'].T  # This to have (samples, traces)
        eve['name'] = filename

        eve['ntr'] = N

        # Get acquisition information from header
        # -----------------
        # Added on Novemeber 24, 2021 by Daniel Escallón
        M = fhead['M']  # Size of trace descriptor block
        f.seek(M, 0)  # Go to header file information after trace descriptor
        hsub = f.read(600)
        eve['ACQUISITION_DATE'] = _return_inside(re.search(r'(?<=x00ACQUISITION_DATE )\d+/\w+/\d+', str(hsub)))
        eve['ACQUISITION_TIME'] = _return_inside(re.search(r'(?<=x00ACQUISITION_TIME )\d+:\d+:\d+', str(hsub)))
        # -----------------
        # -----------------
        # Added on July 15, 2022 by Daniel Escallón
        if _all_equal(sra):
            eve['sra'] = sra[0]
        else:
            print('Not all sampling rates for traces are the same. List is stored instead')
            eve['sra'] = sra
        if _all_equal(delay):
            eve['delay'] = delay[0]
        else:
            print('Not all delays for traces are the same. List is stored instead')
            eve['delay'] = delay
        if _all_equal(nsamp):
            eve['nsamp'] = nsamp[0]
        else:
            print('Not all number of samples for traces are the same. List is stored instead')
            eve['nsamp'] = nsamp

        eve['COMPANY'] = _return_inside(re.search(r'(?<=x00COMPANY )[\w\d\s.]*', str(hsub)))
        eve['INSTRUMENT'] = _return_inside(re.search(r'(?<=x00INSTRUMENT )[\w\d\s.]*', str(hsub)))
        eve['JOB_ID'] = _return_inside(re.search(r'(?<=x00JOB_ID )[\d.]*', str(hsub)))
        eve['OBSERVER'] = _return_inside(re.search(r'(?<=x00OBSERVER )[\w\d\s]*', str(hsub)))
        eve['SERIAL_STRING'] = _return_inside(re.search(r'(?<=x00SERIAL_STRING )[\w\d\s.]*', str(hsub)))
        eve['TRACE_SORT'] = _return_inside(re.search(r'(?<=x00TRACE_SORT )[\w\d\s]*', str(hsub)))
        eve['UNITS'] = _return_inside(re.search(r'(?<=x00UNITS )[\w\d]*', str(hsub)))

        nt = _return_inside(re.search(r'\b(x00NOTE ).*\\[^(x)]', str(hsub)))
        note = {}
        note['BASE_INTERVAL'] = _return_inside(re.search(r'(?<=BASE_INTERVAL )[\w\d.]*', str(nt)))
        note['SHOT_INCREMENT'] =_return_inside( re.search(r'(?<=SHOT_INCREMENT )[\w\d.]*', str(nt)))
        note['PHONE_INCREMENT'] = _return_inside(re.search(r'(?<=PHONE_INCREMENT )[\w\d.]*', str(nt)))
        note['AGC_WINDOW'] = _return_inside(re.search(r'(?<=AGC_WINDOW )[\w\d.]*', str(nt)))
        note['DISPLAY_FILTERS'] = _return_inside(re.search(r'(?<=DISPLAY_FILTERS )[\w\d.\s]*', str(nt)))
        eve['NOTE'] = note

        traces_info = {}
        traces_info['nsamp'] = nsamp
        traces_info['SAMPLE_INTERVAL'] = sra
        traces_info['SAMPLE_INTERVAL'] = sra
        traces_info['DELAY'] = delay
        traces_info['ALIAS_FILTER'] = al_fl
        traces_info['AMPLITUDE_RECOVERY'] = amp_recov
        traces_info['CHANNEL_NUMBER'] = chan_num
        traces_info['DESCALING_FACTOR'] = desc_fac
        traces_info['DIGITAL_HIGH_CUT_FILTER'] = dig_high_cut
        traces_info['DIGITAL_LOW_CUT_FILTER'] = dig_low_cut
        traces_info['FIXED_GAIN'] = fx_gain
        traces_info['LINE_ID'] = line_id
        traces_info['LOW_CUT_FILTER'] = low_cut_fl
        traces_info['NOTCH_FREQUENCY'] = notch_freq
        traces_info['RAW_RECORD'] = raw_record
        traces_info['RECEIVER_LOCATION'] = rec_loc
        traces_info['SHOT_SEQUENCE_NUMBER'] = sh_seq_num
        traces_info['SKEW'] = skew
        traces_info['SOURCE_LOCATION'] = sou_loc
        traces_info['STACK'] = stack
        traces_info['NOTE'] = note_tr

        eve['TRACES_INFO'] = traces_info
        # -----------------

    return eve

