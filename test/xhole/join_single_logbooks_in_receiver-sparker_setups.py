import os, sys
import numpy as np
import pandas as pd
from gdp.helpers.common_functions import convert_str_to_int
from gdp.import_export.rgeod3 import rgeod3

#%%

path_seis = os.path.abspath('/home/daniel/ETHZ_Polybox/Shared/crosshole-surveys/AS') + os.sep

for path, subdirs, files in os.walk(path_seis):
    for name in files:
        sys.path.append(path+os.sep+name)

logbooks = path_seis + 'field-logbooks' + os.sep

# load all files
fi = os.listdir(logbooks)

# select only the ones with the sheets
file_names = [file for file in fi if file.split('.')[-1] == 'ods' and file.split('.')[0][-5:] == 'sheet']

# %% create excel for dd format matlab

receiver_setup = pd.DataFrame(columns=['Setup no.',
                                      'ETH Streamer borehole',
                                      'ETH streamer depth',
                                      'GT Streamer borehole',
                                      'GT streamer depth'])

sparker_setup = pd.DataFrame(columns=['Sparker borehole',
                                       'Sparker depth',
                                       'File ID Start',
                                       'File ID End',
                                       'Receiver setup',
                                       'Comments'])

for count, file in enumerate(file_names):
    # with open(logbooks + file) as f:

    streamer_pos = pd.read_excel(logbooks + file, header=7, nrows=2, index_col=0)

    rc = pd.DataFrame({'Setup no.': count + 1,
                       'ETH Streamer borehole': streamer_pos.loc['ETH Streamer', ('Borehole')].replace(" ", ""),
                       'ETH streamer depth': convert_str_to_int(streamer_pos.loc['ETH Streamer', ('Marker @ flange')]),
                       'GT Streamer borehole': streamer_pos.loc['GT Streamer', ('Borehole')].replace(" ", ""),
                       'GT streamer depth': convert_str_to_int(streamer_pos.loc['GT Streamer', ('Marker @ flange')])
                       }, index = [count +1])

    receiver_setup = receiver_setup.append(rc)

    f = pd.read_excel(logbooks + file, header=11)

    sp = pd.DataFrame()
    sp['Sparker depth'] = [convert_str_to_int(val) for val in f['Sparker Depth']]
    sp['File ID Start'] = f['File ID Start']
    sp['File ID End'] = f['File ID End']
    sp['Comments'] = f['Comments']
    sp['Receiver setup'] = count + 1
    sp['Sparker borehole'] = pd.read_excel(logbooks + file, header=4, nrows=1).iloc[0, 0][-3:]

    sparker_setup = sparker_setup.append(sp)