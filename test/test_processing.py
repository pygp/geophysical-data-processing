import os
import matplotlib.pyplot as plt
import numpy as np
from gdp import TEST_DIR


def test_gain():
    x = np.linspace(-5, 5, 1544)
    y = np.linspace(-5, 5, 5000)
    xx, yy = np.meshgrid(x, y, sparse=True)
    data = 0.1*(np.sin(xx ** 2 + yy ** 2) / (xx ** 2 + yy ** 2))
    plt.figure()
    plt.imshow(data, aspect='auto')
    plt.colorbar()
    from gdp.processing import gain
    linear_gain = gain.apply_gain(data, sfreq=1, exponent=10, gain_type='linear')
    spherical_gain = gain.apply_gain(data, sfreq=1, gain_type='spherical', exponent=10, velocity=1)
    plt.figure()
    plt.imshow(linear_gain[0], aspect='auto')
    plt.colorbar()
    plt.figure()
    plt.imshow(spherical_gain[0], aspect='auto')
    plt.colorbar()
    plt.show()

def test_agc():
    x = np.linspace(-5, 5, 1544)
    y = np.linspace(-5, 5, 5000)
    xx, yy = np.meshgrid(x, y, sparse=True)
    data = 0.1 * (np.sin(xx ** 2 + yy ** 2) / (xx ** 2 + yy ** 2))
    plt.figure()
    plt.imshow(data, aspect='auto')
    plt.colorbar()

    from gdp.processing.gain import agc_2D

    agc_gain = agc_2D(data)
    plt.figure()
    plt.imshow(agc_gain, aspect='auto')
    plt.colorbar()
    plt.show()


def test_normalize():
    x = np.linspace(-5, 5, 500)
    y = np.linspace(-5, 5, 1000)
    xx, yy = np.meshgrid(x, y, sparse=True)
    data = 0.1 * (np.sin(xx ** 2 + yy ** 2) / (xx ** 2 + yy ** 2))

    fig, ax = plt.subplots(3,3)
    im = ax[0, 0].imshow(data, aspect='auto')
    ax[0, 0].set_title('Original')
    fig.colorbar(im, ax=ax[0, 0])

    from gdp.processing.normalizing import normalize_data

    options = ['tracewise-max-abs',
               'tracewise-rms',
               'tracewise-max-abs-window',
               'tracewise-rms-window',
               'max-abs',
               'rms-sum',
               'max',
               'rms'
               ]
    xc = 1
    yc = 0
    for i, opt in enumerate(options):
        dat = normalize_data(data.copy(), opt, window=(10, 20))
        im = ax[xc, yc].imshow(dat, aspect='auto')
        ax[xc, yc].set_title(opt)
        fig.colorbar(im, ax=ax[xc, yc])
        xc += 1
        if xc == 3:
            xc = 0
            yc += 1
        del im, dat
    plt.tight_layout()
    plt.show()

def test_clip_interpolation():
    from gdp import DATA_DIR
    from gdp.import_export.import_gpr_data import load_mala
    from gdp.processing.general import interpolate_clipping
    import matplotlib.pyplot as plt
    gpr_data, header = load_mala(DATA_DIR + 'raw-data/raw-data/200728_ST1_100MHz/DAT_0862_A1.rd3')
    no_clip = interpolate_clipping(gpr_data)
    # compare before and after for a single trace
    plt.plot(gpr_data[:, 100])
    plt.plot(no_clip[:, 100])
    plt.show()