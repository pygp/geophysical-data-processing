import matplotlib.pyplot as plt
import numpy as np


def test_interpolate_to_new_size():
    x = np.arange(-5, 5, 0.1)
    y = np.arange(-5, 5, 0.1)

    xx, yy = np.meshgrid(x, y, sparse=True)
    z = np.sin(xx ** 2 + yy ** 2) / (xx ** 2 + yy ** 2)

    from gdp.interpolation import interpolate_to_new_size

    new_z = interpolate_to_new_size(z, size=(500, 500))
    assert new_z.shape == (500, 500)

    fig, ax = plt.subplots(1,2)
    ax[0].imshow(z)
    ax[1].imshow(new_z)
    plt.show()

    new_z = interpolate_to_new_size(z, size=(20, 20))
    assert new_z.shape == (20, 20)

    fig, ax = plt.subplots(1, 2)
    ax[0].imshow(z)
    ax[1].imshow(new_z)
    plt.show()


def test_interpolate_missing_traces():
    x = np.arange(-5, 5, 0.1)
    y = np.arange(-5, 5, 0.1)

    xx, yy = np.meshgrid(x, y, sparse=True)
    z = np.sin(xx ** 2 + yy ** 2) / (xx ** 2 + yy ** 2)
    z_in = z.copy()
    fig, ax = plt.subplots(2, 2)
    ax[0, 0].imshow(z)
    ax[0, 0].set_title('Real Data')

    z_in[:, 1::4] = np.nan
    ax[0, 1].imshow(z_in)
    ax[0, 1].set_title('Missing traces')

    from gdp.interpolation import interpolate_missing_traces

    z_out = interpolate_missing_traces(z_in, method='cubic')

    ax[1, 0].imshow(z_out)
    ax[1, 0].set_title('Interpolated missing traces')

    ax[1, 1].imshow(z-z_out, cmap='seismic')
    ax[1, 1].set_title('Difference')

    plt.tight_layout()
    plt.show()



