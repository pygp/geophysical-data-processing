from gdp.import_export.rgeod3 import rgeod3
from gdp.processing.normalizing import normalize_data
from gdp.processing.filtering import filter_data
from gdp.processing.general import trim_traces
from gdp.plotting import plot_trace_freq, plot_trace_time, plot_frequency_information
import matplotlib.pyplot as plt
import matplotlib
#matplotlib.use('Qt5Agg')
import numpy as np

path = '/home/daniel/ETHZ_Polybox/Shared/Tripode_experiment_wave_attenuation/data_raw/'
d = rgeod3(path + '3060.dat')


#plt.imshow(d['data'], aspect='auto'), plt.colorbar(), plt.show()

#plt.plot(d['data'][:, 10]), plt.show()


#data = d['data']
#data = trim_traces(data,
#                   time=-0.013, #d['delay'],
#                   sample_rate=d['sra'])
#


data = d['data'][:,:24]
data = normalize_data(data, typ='tracewise-max-abs')

extent = (0, data.shape[1], len(data) * d['sra'], 0)
plt.figure()
plt.imshow(data, aspect='auto', extent=extent, 
           clim=(-0.2,0.2), cmap='seismic'), plt.colorbar(), plt.show()

fig = plot_frequency_information(data[:,20], d['sra'], figsize=(20,10))



#%%
fq = 5000 #(5000, 10_000)
data = filter_data(data,
                   fq=fq,
                   btype='highpass',
                   sfreq=1/d['sra'])

fig = plot_frequency_information(data[:,20], d['sra'], figsize=(20,10))

#%%
#data = normalize_data(data, typ='tracewise-rms')

data = data#[:1000]#, 24:31]

trace1 = data[:, 27]
plt.figure()
plt.plot(np.linspace(0, len(trace1) * d['sra'], len(trace1)), trace1), plt.show()
trace2 = data[:, 10]
plt.figure()
plt.plot(np.linspace(0, len(trace2) * d['sra'], len(trace2)), trace2), plt.show()


extent = (0, d['ntr'], len(data) * d['sra'], 0)
plt.figure()
plt.imshow(data, aspect='auto', extent=extent, ), plt.colorbar(), plt.show()





#%%
path = '/home/daniel/Downloads/'
path = '/home/daniel/Downloads/BAP-P_waves/Rawdata/20220511_tripod/'
d = rgeod3(path + '1042.dat')