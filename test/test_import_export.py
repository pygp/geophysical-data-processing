import os
import matplotlib.pyplot as plt


def test_load_mala():
    from gdp.import_export import import_gpr_data
    dat, info = import_data.load_mala('test_data/DAT_1324_A1.rd3')
    print(info)
    plt.imshow(dat, aspect='auto', clim=(2500,2800))
    plt.colorbar()
    plt.show()

def test_load_dt1():
    from gdp.import_export import import_gpr_data
    path_to_data = '/home/alexis/Desktop/PfynwaldFieldwork/GPR_trees/data/'
    dat, info = import_data.load_dt1(path_to_data + 'LINE51')
    print(info)
    plt.imshow(dat, aspect='auto')
    plt.colorbar()
    plt.show()

def test_load_seg2():
    from gdp.import_export.rgeod3 import rgeod3
    tr_data = rgeod3('test_data/999.dat')
    print(tr_data)
    plt.imshow(tr_data['data'], aspect='auto')
    plt.show()
    tr_data = rgeod3('test_data/1071.dat')
    print(tr_data)
    plt.imshow(tr_data['data'], aspect='auto')
    plt.show()
