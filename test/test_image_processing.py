from gdp.processing.image_processing import remove_svd
from matplotlib import pyplot as plt


def test_svd_removal():
    from gdp.import_export import load_mala
    dat, info = load_mala('test_data/DAT_1324_A1.rd3')
    no_svd, svd_removed = remove_svd(dat, 1, 5)
    fig, axs = plt.subplots(3, 1)
    axs[0].imshow(dat)
    axs[1].imshow(no_svd)
    axs[2].imshow(svd_removed)
    axs[0].set_title('Input data')
    axs[1].set_title('Input data - SVD')
    axs[2].set_title('SVD')
    plt.show()


def test_pick_roi():
    from gdp.import_export import load_mala
    from gdp.processing.filtering import filter_data
    from gdp.processing.image_processing import pick_roi_in_image
    dat, info = load_mala('test_data/DAT_1324_A1.rd3')
    data = filter_data(dat, 10, info['frequency'], btype='highpass')
    extent = (0, 5, 0, 1)
    points = pick_roi_in_image(image=data, extent=extent, save_image_crop=True)
    for key, values in points.items():
        plt.figure()
        plt.imshow(values['data'])
        plt.title(key)
        plt.show()


def test_pick_image(data_source: str = 'real'):
    import numpy as np
    import matplotlib.pyplot as plt
    from gdp.processing.image_processing import pick_line
    if data_source == 'synthetic':
        N = 256
        x = np.linspace(-np.pi, np.pi, N)
        sine1D = np.sin(x * 8.0)
        data = np.ndarray((N, N))
        for i in range(N):
            data[i] = np.roll(sine1D, -i)
    elif data_source == 'real':
        from gdp.import_export import load_mala
        from gdp.processing.filtering import filter_data
        dat, info = load_mala('test_data/DAT_1324_A1.rd3')
        data = filter_data(dat, 10, info['frequency'], btype='highpass')
    else:
        print(' Data source: choose real or synthetic')
        return
    extent = (0, 1, 0, 1)
    output = pick_line(data,
                       extent=extent,
                       colormap='seismic',
                       interpolate_path=True,
                       automatic_naming=True,
                       compute_lengths=True,
                       compute_zero_crossing=0.2,
                       values_along_path=True)
    fig, axs = plt.subplots(2, 1)
    im = axs[0].imshow(data,
                       extent=extent)
    for key, item in output.items():
        transform = item['true coordinates']
        if transform.any():
            axs[0].plot(transform[:, 0], transform[:, 1])
            axs[1].plot(item['path distance'], item['path values'])
    plt.show()


def test_pick_reflector_image_no_noise():
    from gdp.processing.image_processing import pick_first_arrival
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib
    matplotlib.use('Qt5Agg')

    file = np.load('/home/daniel/GitProjects/geophysical-data-processing/test/test_data/time_response.npz')
    time_response = file['time_response']
    time_vector = file['time_vector']
    time_response = (time_response / np.max(np.abs(time_response), axis=1)[:, None]).T
    plt.figure()
    plt.imshow(time_response, aspect='auto', cmap='seismic')
    plt.colorbar()
    plt.show()

    l = pick_first_arrival(time_response, threshold=0.1)
    plt.figure()
    plt.imshow(time_response, aspect='auto', cmap='viridis')
    plt.plot(l[:, 0], l[:, 1], 'r.')
    plt.colorbar()
    plt.show()
