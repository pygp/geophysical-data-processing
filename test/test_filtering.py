import numpy as np
import matplotlib.pyplot as plt

def test_filter():
    x = np.linspace(-5, 5, 1544)
    y = np.linspace(-5, 5, 5000)
    xx, yy = np.meshgrid(x, y, sparse=True)
    data = 0.1*(np.sin(xx ** 2 + yy ** 2) / (xx ** 2 + yy ** 2))
    plt.figure()
    plt.imshow(data, aspect='auto')
    plt.colorbar()
    plt.show()
    from gdp.processing.filtering import filter_data
    fq = 0.1
    sfreq = 10
    btype = 'highpass'
    data = filter_data(data,
                       fq,
                       sfreq,
                       btype,
                       )
    plt.figure()
    plt.imshow(data, aspect='auto')
    plt.colorbar()
    plt.show()

def test_dewow():
    from gdp.processing.filtering import dewow
    n_samples = 1000
    n_traces = 10
    np.random.seed(0)
    data = 0.2 * np.random.randn(n_samples, n_traces) +  \
            np.linspace(0, 1, n_traces)[None] +  \
            np.sin(np.linspace(0, 20, n_samples))[..., None]




    # Apply dewow with a window length of 50 samples
    window_length = 50
    dewowed_data = dewow(data, window_length)

    # Plot the original and dewowed signals (first trace for illustration)
    fig, ((ax0, ax1),(ax2, ax3)) = plt.subplots(2, 2, figsize=(10, 6))
    ax0.imshow(data, aspect='auto')

    ax1.imshow(dewowed_data, aspect='auto')

    ax2.plot(data[:, 5], label='Original Data with Drift')
    ax3.plot(dewowed_data[:, 5], label='Dewowed Data')
    fig.legend()
    # fig.title('Dewow Example')
    plt.show()

def test_dewow_real_data():
    from gdp.processing.filtering import dewow
    from gdp.import_export import load_mala
    from gdp.processing.filtering import dewow
    
    # input_data = load_mala(DATA_DIR + os.sep + 'MALA' + os.sep + '210226_ST1_100MHz' + os.sep + 'DAT_1112_A1.rd3')
    dat, info = load_mala('test_data/DAT_1324_A1.rd3')
    window_length = 100
    dewowed_data = dewow(dat, window_length)

    fig, (ax,ax1) = plt.subplots(2,1)
    cax = ax.imshow(dat, aspect='auto')
    cax1 = ax1.imshow(dewowed_data[window_length:-window_length], aspect='auto')
    ax.set_title('Input data')
    ax2.set_title('Dewow')
    fig.colorbar(cax, ax=ax)
    fig.colorbar(cax1, ax=ax1)
    plt.show()