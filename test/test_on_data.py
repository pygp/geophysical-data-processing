import os
import matplotlib.pyplot as plt
from gdp import TEST_DIR


def test_interpolate_missing_traces():
    # path_to_data = os.path.abspath(os.path.expanduser("~")+'/GitProjets/FracWave/data/GPR_Raw/data/') + os.sep
    from gdp.import_export import load_mala
    dat, info = load_mala(TEST_DIR + 'DAT_1324_A1.rd3')

    plt.imshow(dat, aspect='auto', clim=(2500, 2800))
    plt.colorbar()
    plt.show()

    from gdp.interpolation import interpolate_missing_traces

    dat = interpolate_missing_traces(dat, method='linear')
    plt.imshow(dat, aspect='auto', clim=(2500, 2800))
    plt.colorbar()
    plt.show()

from obspy.core.stream import Stream




def test_agc_on_data():
    from gdp.import_export import load_mala
    from gdp.processing import gain, filtering
    from gdp.interpolation import interpolate_missing_traces
    dat, info = load_mala(TEST_DIR + 'DAT_1324_A1.rd3')
    #    from gdp.interpolation import interpolate_missing_traces
    dat = filtering.detrend(dat)

    plt.imshow(dat, aspect='auto')
    plt.colorbar()
    plt.show()

    dat = gain.agc(dat)
    plt.imshow(dat, aspect='auto')
    plt.colorbar()
    plt.show()


def test_normalize_data():
    from gdp.import_export import load_mala
    from gdp.processing import filtering
    from gdp.interpolation import interpolate_missing_traces
    #data, info = load_mala(TEST_DIR + 'DAT_1324_A1.rd3')
    #data = interpolate_missing_traces(data, 'nearest')
    #data = filtering.detrend(data)
    from gdp.import_export.rgeod3 import rgeod3
    dat = rgeod3(TEST_DIR + '1071.dat')
    data = dat['data'][:, 1:]  # delete the first trace corresponding to trigger shot

    fig, ax = plt.subplots(3, 3, figsize=(20, 20))
    im = ax[0, 0].imshow(data, aspect='auto', cmap='seismic')
    ax[0, 0].set_title('Original')
    fig.colorbar(im, ax=ax[0, 0])

    fig2, ax2 = plt.subplots(3, 3)
    ax2[0, 0].plot(data[:, 10])
    ax2[0, 0].set_title('Original')

    from gdp.processing.normalizing import normalize_data

    options = ['tracewise-max-abs',
               'tracewise-rms',
               'tracewise-max-abs-window',
               'tracewise-rms-window',
               'max-abs',
               'rms-sum',
               'max',
               'rms'
               ]
    xc = 1
    yc = 0
    for i, opt in enumerate(options):
        dat = normalize_data(data.copy(), opt, window=(10, 20))
        im = ax[xc, yc].imshow(dat, aspect='auto', cmap='seismic')
        ax[xc, yc].set_title(opt)
        fig.colorbar(im, ax=ax[xc, yc])

        dat_t = normalize_data(data[:, 10].copy(), opt, window=(10, 20))

        ax2[xc, yc].plot(dat_t)
        ax2[xc, yc].set_title(opt)
        xc += 1
        if xc == 3:
            xc = 0
            yc += 1
        del im, dat
    fig.tight_layout()
    fig2.tight_layout()
    plt.show()
