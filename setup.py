from setuptools import setup, find_packages
version = '1.1.0'

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='geophysical_data_processing',
    version=version,
    packages=find_packages(exclude=('tests', 'docs', 'user')),
    include_package_data=True,
    install_requires=[
        'numpy',
        'matplotlib',
        'pandas',
        'scipy',
        'scikit-image',
        'pytest',
        'pyvista',
        'opencv-python',
        'requests',
        'cmapy',
        'segyio',
    ],
    url='https://gitlab.com/pygp/geophysical-data-processing',
    license='LGPL v3',
    author='Alexis Shakas, Daniel Escallon',
    author_email='alexis.shakas@eaps.ethz.ch, '
                 'daniel.escallon@eaps.ethz.ch',
    description='Tools for pre and post processing various geophysical data',
    keywords=['Seismic', 'GPR']
)
