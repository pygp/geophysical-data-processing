# Geophysical Data Processing

This package offers a series of functions that are made to facilitate the processing of Seismic and/or GPR data. 

# Local installation

```
git clone https://gitlab.com/pygp/geophysical-data-processing.git
cd geophysical-data-processing
```

# Optional: create a conda (or python) environment
```
conda create --name gdp python=3.12
conda activate gdp
```


Local installation:
```
pip install -e .
```

# Pip installation
```
pip install -e git+https://gitlab.com/pygp/geophysical-data-processing.git@main#egg=geophysical-data-processing
```

# Upgrade
```
pip install --upgrade -e git+https://gitlab.com/pygp/geophysical-data-processing.git@main#egg=geophysical-data-processing
```
